## Python Programming for Data Science II <img src="oudce_logo.png" align="right"/>

### Massimiliano Izzo 

Materials for [Python Programming for Data Science Part II](https://www.conted.ox.ac.uk/courses/python-programming-for-data-science-part-2?code=O22P490COW) - **this page will be updated as the course progresses**.

The class workspace on **Slack** is https://pp4ds-ox.slack.com. I encourage you to ask questions should you have them in the Slack channel incase your classmates can help. Massi (your tutor; massimiliano.izzo@conted.ox.ac.uk) will also check Slack and provide support where possible. Download Slack from: https://slack.com/get

If you do not wish to use slack you can use Canvas to contact me and other students. 

To use **Jupyter** yourself, I recommend you download and install **Anaconda**, a Python Data Science Platform, from: [here](https://www.anaconda.com) Make sure you download the **Python 3** version of Anaconda, ideally the latest version for Python 3.10. You can also install Jupyter if you have a standard Python distribution installed. Ask your tutors for assistance if you need to install Jupyter on your own machine.

To get the contents of this repository I recommend that you install **Git SCM**, a source code management software, that will help you keep up-to-date with the repository. I will be adding content as the course progresses and Git will allow you to pull new material as it becomes available.

### Cloning this repository to use it offline

If you want to run the notebooks on your own computer at home, apart from installing Jupyter/Anaconda as per above, you will need to install **Git**, which is a source code management software, from [here](https://git-scm.com/downloads). Windows users can also get Git here: https://gitforwindows.org/. Once installed, you need to open up the command-line ("Command Prompt" on Windows or "Terminal" on Mac OSX) to run some commands.

Change directory to somewhere sensible, such as `My Documents` or similar on Windows or `Documents` on Mac OSX. Assuming you're using `Documents`:

```
cd Documents
```

Then ask Git to clone this repository with the following command.
```
git clone https://gitlab.com/data-science-course/pp4ds-pt2-tt2023.git
```
(or, in the unlikely case that you have SSH enabled use `git clone git@gitlab.com:data-science-course/pp4ds-pt2-tt2023.git`)

This will create a subdirectory called `pp4ds-pt2-tt2023` in your `Documents` folder. When you need to update the content at some later time after I have added some new files to the repository, you will need to open up the command-line again and do the following commands.
```
cd Documents/pp4ds-pt2-tt2023
git pull
```
What this does is to ask Git to check if there are any new changes in the online repository and to download those new files or updates to the existing files.

Either some lines of stuff should whizz by, or it will say `Already up to date.` if there are no new changes.

If this doesn't work, you may need to force the update, which will overwrite your local files. To do this (make sure any of your own work is renamed or moved outside of the `pp4ds-pt2-tt2023` folder first):
```
git fetch --all
git reset --hard origin/master
```

### Course Programme (inidicative)

**Week 1:** Introduction to the course. Basic overview of Machine Learning. Linear Regression example.

**Week 2:** Overview of a data-science pre-processing pipeline. Exploratory Data Analysis

**Week 3:** Data cleaning and preparation.

**Week 4:** Supervised Learning: regression.

**Week 5:** Supervised Learning: classification. (May 23)

**Week 6:** Supervised Learning: classification. (May 30)

**Week 7:**  Decision Trees. Ensemble Methods. Hyperparameter Tuning. (June 6)

**Week 8:**  Dimensionality reduction and Unsupervised Learning. (June 13)

**Week 9:**  The Perceptron. Back-propagation. Fully-connected neural networks. (June 27)

**Week 10:**  Deep Learning: fundamental concepts. Convolutional Neural Networks (CNNs) for Image Processing. (July 4)

## Week 1: Introduction to the course

* Lecture notes: [download](https://tinyurl.com/w7ftzxkk) 
* Intro Exercise 01A: **Notebook Basic** 
* Intro Exercise 01B: **Running Code** 
* Intro Exercise 01C: **Working with Markdown** 
* Intro Exercise 01D: **Notebook Exercises** 
* Exercise 01: **Matrix Operations recap, Linear Regression** 

## Week 2: Exploratory data analysis

* Lecture notes: [download](https://tinyurl.com/2p8a2uf5) 
* Live Demo 02: **Exploratory Data Analysis (EDA) on the Kings County Housing Dataset** 
* Exercise 01: **solutions available**

## Week 3: Data cleaning and preparation. Regression.

* Lecture notes: [download](https://tinyurl.com/mwtj2eu8) 
* Live Demo 02: **solutions available**
* Live Demo 03A: **Data cleaning and preparation on the Kings County Housing Dataset**

## Week 4: Regression.

* Lecture notes: same as the previous week.
* Live Demo 03A: **solutions available**
* Live Demo 04: **Regression on the Kings County Housing Dataset**

## Week 5: Binary Classification

* Lecture notes: [download](https://tinyurl.com/tc64mncd)
* Live Demo 05: **Binary Classification with the MNIST dataset**
* First Assignment: quiz (see the `assignments` folder)

## Week 6: Multi-class and Multi-label Classification

* Live Demo 04: **solutions available**
* Live Demo 06: **Multi-class and Multi-label Classification with the MNIST dataset**

## Week 7: Decision Trees, Ensemble Methods, Hyperparameter Tuning

* Lecture notes: [download](https://tinyurl.com/397d2uty)
* Live Demo 07A: **Decision Trees for classification**
* Live Demo 07B: **Ensemble Methods for classification**
* Live Demo 07C: **DT and EM for regression on the Kings County dataset; Hyperparameter Tuning**
* Second Assignment: IRIS dataset (see the `assignments` folder)

## Week 8: Unsupervised Learning

* Lecture notes: [download](https://tinyurl.com/jvmcdkff)
* Live Demo 08A: **Dimensionality reduction**
* Live Demo 08B: **Clustering**

## Week 9: Introduction to Neural Networks with PyTorch (Lightning)

* Lecture notes: [download](https://tinyurl.com/4ne8dur9)
* Live demo 09: **PyTorch Tensors. Multi-class classification on the FashionMNIST dataset** (https://tinyurl.com/4j3ecy6s)
* Third Assignment: personal project

## Week 10: Deep Networks with PyTorch (Lightning)

* Lecture notes: [download](https://tinyurl.com/4jkfw2f4)
* Live demo 10A: **Training deep networks**
* Live demo 10B: **Intro to Convolutions. Convolutional Neural Networks on the FashionMNIST dataset** (https://tinyurl.com/3t3jtaye)
