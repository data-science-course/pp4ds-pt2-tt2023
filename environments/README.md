## Installing this environment

This environment contains all the dependencies we will need to do the "Deep Learning" classes.

To install it open an "Anaconda Prompt" (in Windows) or a terminal (in Mac/Linux).

Navigate to this directory (e.g `cd ~/Documents/pp4ds-pt2-tt2023/environments` if you have cloned the Gitlab project in `Documents`) and run

```bash
conda env create -f data-science-cpuonly.yaml
```

It may take a while so be patient.