{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Training Deep Neural Networks"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This notebook is heavily inspired by Andre Guernon work, that can be found here: https://github.com/ageron/handson-ml/blob/master/11_deep_learning.ipynb"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To tackle some very complex problems, you may have to design and train a very deep network (> 10 layers), with complex connectivity. This could generate a variety of issues\n",
    "* vanishing / exploding gradients\n",
    "* too little data or not enough labels for our data\n",
    "* training could be very time-consuming\n",
    "* there would be a high risk of overfitting your data\n",
    "\n",
    "Let's see, one by one, how we can sort out these issue"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Setup"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Python ≥ 3.8 is required\n",
    "import sys\n",
    "assert sys.version_info >= (3, 8)\n",
    "\n",
    "# Scikit-Learn ≥0.20 is required\n",
    "import sklearn\n",
    "assert sklearn.__version__ >= \"1.0\"\n",
    "\n",
    "# Common imports\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import os\n",
    "\n",
    "# To plot pretty figures\n",
    "%matplotlib inline\n",
    "import matplotlib as mpl\n",
    "import matplotlib.pyplot as plt\n",
    "mpl.rc('axes', labelsize=14)\n",
    "mpl.rc('xtick', labelsize=12)\n",
    "mpl.rc('ytick', labelsize=12)\n",
    "\n",
    "# Ignore useless warnings (see SciPy issue #5998)\n",
    "import warnings\n",
    "warnings.filterwarnings(action=\"ignore\", message=\"^internal gelsd\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import torch"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Vanishing/Exploding Gradients"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The _vanishing gradient_ problem happens when training artificial neural networks with gradient-based learning methods and backpropagation. In such methods, each of the neural network's weights receives an update proportional to the partial derivative of the error function with respect to the current weight in each iteration of training. The problem is that in some cases, the gradient will be vanishingly small, effectively preventing the weight from changing its value. In the worst case, this may completely stop the neural network from further training. This will be especially true as the error back-propagates to the lower layers.\n",
    "\n",
    "In other cases, however, it can happen exactly the opposite: the gradients get bigger and bigger, with consequently very large weight updates, and a divergence of the algorithm. This is called the _exploding gradients_ problem.\n",
    "\n",
    "Some of the causes of unstable gradients were found by Glorot and Bengio in a paper published in 2010. They found that the sigmoid activation function together with a popular weight initialization technique used at that time caused a huge variance in each layer output, and this variance tended to increase layer by layer.\n",
    "\n",
    "When inputs have a large absolute value, the sigmoid function saturates (to 0 in input negative, to 1 if positive), and the derivative tends to zero. Hence, there is no gradient to backpropagate through the network.\n",
    "\n",
    "For the signal to flow properly across the network both forwards and backwards, the variance of the outputs must be equal to the variance of the inputs. In order to achieve this the weights must be initialized according to _Glorot Initialization_ rule:\n",
    "\n",
    "$$ Normal(0, \\frac{1}{fan_{avg}}) $$\n",
    "\n",
    "or\n",
    "\n",
    "$$ Uniform(-\\sqrt{\\frac{3}{fan_{avg}}}, \\sqrt{\\frac{3}{fan_{avg}}})$$\n",
    "\n",
    "where\n",
    "\n",
    "$$fan_{avg} = \\frac{fan_{in} + fan_{out}}{2}$$\n",
    "\n",
    "and \n",
    "\n",
    "$fan_{in}$ is the number of inputs and $fan_{out}$ is the number of neurons (i.e. of outputs) for each layer\n",
    "\n",
    "Other types of initialization are suggested for other activation functions:\n",
    "\n",
    "|Initialization |Activation functions         |$\\sigma^2$(Normal)\n",
    "|---------------|-----------------------------|-------------------\n",
    "|Glorot         |None, sigmoid, tanh, softmax | $1/fan_{avg}$\n",
    "|He             |ReLU and    its variants     | $2/fan_{in}$\n",
    "|LeCun          |SELU                         | $1/fan_{in}$\n",
    "\n",
    "PyTorch adopts LeCun initialization by default. You can use Glorot initialization by invoking `torch.nn.init.xavier_uniform_()` or `torch.nn.init.xavier_normal()` on each layer weights (see example below). He initialization can be achieved using `torch.nn.init.kaiming_uniform_()` or `torch.nn.init.kaiming_normal_()` instead. You would do this at the end of your constructor method (i.e. `__init__()` hook). For more initialization strategies see the `torch.nn.init` module."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import torch.nn as nn \n",
    "layer = nn.Linear(in_features=100, out_features=50) \n",
    "nn.init.xavier_uniform_(layer.weight)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Glorot and Bengio found out that ReLu works much better than sigmoid and tanh. However ReLu has its problems: (1) during training many neurons \"die\", i.e. start outputting only zeros. This happens especially with a very high learning rate. This happens, because the ReLu outputs zero for all negative values.\n",
    "\n",
    "To solve this issue a few variations of ReLU have been proposed:\n",
    "* Leaky ReLu\n",
    "* Randomized Leaky ReLu (RReLU)\n",
    "* Parametric Leaky ReLu (PReLU)\n",
    "* Exponential Linear Unit (ELU) [Djork-Arné Clevert et al. (2015)]\n",
    "* Scaled ELU (SELU) [Günter Klambauer et al. (2017)]\n",
    "\n",
    "If a neural network is built exclusively of a stack of dense layers, and all the hidden layers use the SELU activation function, then the network will self-normalize. This means that the output of each layer will tend to preserve a mean of 0 and standard deviation of 1 during training, thus solving the issue of the vanishing/exploding gradients. Hence, the SELU activation function often significantly outperforms other activation functions for such neural nets (especially deep ones). There are, however, some conditions that are needed for self-normalization to happen:\n",
    "\n",
    "* Inputs must be standardized, with mean 0 and std 1\n",
    "* hidden layer’s weights must be initialized with LeCun normal initialization. \n",
    "* the network’s architecture must be sequential. This won't work with recurrent networks, or other more complicated architectures\n",
    "* in theory all layers must be dense, however SELU improves performance in convolutional neural networks as well.\n",
    "\n",
    "As an overall rule for the choice of activation functions: SELU > ELU > leaky ReLU (and its variants) > ReLU > tanh > logistic. However, if the network architecture prevents self-normalization, ELU might perform better than SELU.If speed is a priority many libraries and hardware accelerators provide ReLU-dadicated optimizations. Hence, if speed is priority, then ReLU is the choice."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "selu = nn.SELU()\n",
    "batch_size = 5\n",
    "x = torch.rand(batch_size, 100)\n",
    "\n",
    "out = selu(layer(x))\n",
    "out"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import torch.nn.functional as F\n",
    "F.selu(layer(x))"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.2 Batch Normalization\n",
    "\n",
    "Even though He initialization with SELU removes the issue of exploding/vanishing gradients at the beginning of the training it does not guarantee that they won't show again later on during the training phase.\n",
    "A technique called _Batch Normalization_ (proposed by Ioffe and Szegedy, 2015: https://arxiv.org/abs/1502.03167) can solve this problem. \n",
    "\n",
    "\"Batch normalization is achieved through a normalization step that fixes the means and variances of each layer's (hidden layers included) inputs. Ideally, the normalization would be conducted over the entire training set, but to use this step jointly with stochastic optimization methods, it is impractical to use the global information. Thus, normalization is restrained to each mini-batch in the training process.\" (Wikipedia)\n",
    "\n",
    "To zero-center and normalize the inputs, the algorithm needs to estimate the mean value and standard deviation of each input. It does so by evaluating the mean and standard deviation of the input over the current mini-batch of data.\n",
    "\n",
    "The problem comes at test time, when we should evaluate one item at the time and in principle we cannot compute mean and std over mini-batches. Keras's implementation of Batch Normalization estimates these test-time statistics during training. It does so using the moving average (https://www.investopedia.com/terms/m/movingaverage.asp) of the layer’s input means and standard deviations. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import lightning.pytorch as pl\n",
    "\n",
    "class MNISTClassifier(pl.LightningModule):\n",
    "    def __init__(self):\n",
    "        super().__init__()\n",
    "        self.layer_1 = nn.Linear(28 * 28, 128)\n",
    "        self.layer_2 = nn.Linear(128, 256)\n",
    "        self.layer_3 = nn.Linear(256, 10)\n",
    "        self.batch_norm_1 = nn.BatchNorm1d(28 * 28)\n",
    "        self.batch_norm_2 = nn.BatchNorm1d(128)\n",
    "        self.batch_norm_3 = nn.BatchNorm1d(256)\n",
    "        self.softmax = nn.Softmax(dim=1)  # apply softmax to the input dimension\n",
    "\n",
    "    def forward(self, x: torch.Tensor) -> torch.Tensor:\n",
    "        \"\"\"\n",
    "        Forward pass for our Classifier\n",
    "        \"\"\"\n",
    "        batch_size, channels, height, width = x.size()\n",
    "        # print(f\"Batch size = {batch_size}, channels = {channels}, width = {width}, height = {height}\")\n",
    "        # flatten the input image across (channels, height, width)\n",
    "        x = x.view(batch_size, -1)\n",
    "        x = self.layer_1(self.batch_norm_1(x))\n",
    "        x = F.elu(x)\n",
    "        x = self.layer_2(self.batch_norm_2(x))\n",
    "        x = F.elu(x)\n",
    "        x = self.layer_3(self.batch_norm_3((x)))\n",
    "        x = self.softmax(x)\n",
    "        return x"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Each Batch Normalisation layer adds four parameters per input: $\\gamma$, $\\beta$, $\\mu$, and $\\sigma$. The last two parameters are the moving averages and are not trainable with Keras, while the other two are trainable by backpropagation.\n",
    "\n",
    "Ioffe and Szegedy advocated in favour of adding the Batch Normalization layers before the activation functions, not after (as we did above). If you want to do as they suggested, you have to remove the activation function from the dense layer, and add a separate activation layer after the BN step "
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.3 Gradient Clipping\n",
    "\n",
    "Another popular approach to cope with the exploding gradients problem is to clip the gradients during backpropagation so that they never exceed some threshold. This technique is often used in Recurrent Neural Networks, where Batch Normalisation is difficult to perform.\n",
    "\n",
    "In Lightning, implementing Gradient Clipping is just a matter of setting the clip value argument when creating an Trainer object. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from lightning import Trainer\n",
    "trainer = Trainer(gradient_clip_val=0.5)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Reusing pretrained layers\n",
    "\n",
    "### 2.1 Unsupervised Pretraining\n",
    "\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Oftentimes it is cheap to collect unlabeled training examples, but expensive to label them. If you can get plenty of unlabeled training data, you can try to first use it to train some unsupervised network, such as an autoencoder or a generative adversarial network. Afterwards, you an reuse the lower layers of the unsupervised model you've just trained, add the output layer(s) for your task on top of them, and adjust the final network using supervised learning.\n",
    "\n",
    "Geoffrey Hinton adopted this approach in 2006 and wit led to the revival of ANN and the success of Deep Learning. "
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Other approaches involve _self-supervised learning_ which is when you automatically generate the labels from input data itself, then you train a model on the resulting “labeled” dataset using supervised learning techniques. This approach doesn't require human labeling/supervision, so it is most often considered as a form of unsupervised learning. For instance, for natural language processing (NLP) applications, you can get massive corpora of text documents and automatically generate labels from it."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. Faster Optimizers\n",
    "\n",
    "You can find an overview on optimizers here: https://towardsdatascience.com/full-review-on-optimizing-neural-network-training-with-optimizer-9c1acc4dbe78\n",
    "\n",
    "### Reminder: Gradient Descent\n",
    "\n",
    "$$ \\theta^{(t+1)} = \\theta^{(t)} - \\eta \\nabla_{\\theta} J(\\theta) $$"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Momentum Optimization\n",
    "\n",
    "Instead of using only the gradient of the current step to guide the search, momentum optimisation also accumulates the gradient of the past steps to determine the direction to go. \n",
    "\n",
    "$$ m^{(t+1)} = \\beta m^{(t)} - \\eta \\nabla_{\\theta} J(\\theta) $$\n",
    "$$ \\theta^{(t+1)} = \\theta^{(t)} + m $$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model = MNISTClassifier()\n",
    "optimizer = torch.optim.SGD(model.parameters(), lr=0.001, momentum=0.9)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Nesterov Accelerated Gradient\n",
    "\n",
    "$$ m^{(t+1)} = \\theta m^{(t)} - \\eta \\nabla_{\\theta} J(\\theta + \\beta m) $$\n",
    "$$ \\theta^{(t+1)} = \\theta^{(t)} + m $$\n",
    "\n",
    "A variant of momentum optimisation. Nesterov update ends up slightly closer to the optimum. After a while, these small improvements add up and NAG ends up being significantly faster than regular momentum optimization. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "optimizer = torch.optim.SGD(\n",
    "    model.parameters(),\n",
    "    lr=0.001,\n",
    "    momentum=0.9,\n",
    "    nesterov=True\n",
    ")"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Adagrad \n",
    "\n",
    "This algorithm decays the learning rate, but this happens faster for steep dimensions than for dimensions with less pronounced slopes. It does this by accumulating the squares of gradients and scaling the learning rate with respect to the accumulated squares. This is called an adaptive learning rate. Not suitable for Deep Neural Networks"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### RMSProp\n",
    "\n",
    "AdaGrad runs the risk of slowing down a bit too fast and never converging to the global optimum. The RMSProp algorithm fixes this by accumulating only the gradients from the most recent iterations (as opposed to all the gradients since the beginning of training). It does so by using exponential decay by a decay rate $\\alpha$\n",
    "\n",
    "RMSprop uses a (moving) average of squared gradients to normalize the gradient. This normalization balances the step size  (momentum),  decreasing the step for large gradients to avoid exploding, and increasing the step for small gradients to avoid vanishing. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "optimizer = torch.optim.RMSprop(\n",
    "    lr=0.001,\n",
    "    alpha=0.9 # decay rate\n",
    ")"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Adam\n",
    "\n",
    "The *adaptive moment estimation*, combines the ideas of momentum optimization and RMSProp. It keeps track of an exponentially decaying average of past gradients ($\\beta_1$ decay rate) and  of an exponentially decaying average of past squared gradients ($\\beta_2$ decay rate).\n",
    "Being and adaptive learning rate algorithm, you need less tuning of the learning rate, making it more flexible than standard gradient descent.\n",
    "There are two more variants of Adam: AdaMAX (scales down the parameter updates of $\\beta_2$ by the $l_{\\inf}$ norm rather than the $l_{2}$ norm) and Nadam (Adam + Nesterov)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "optimizer = torch.optim.Adam(\n",
    "    model.parameters(),\n",
    "    lr=0.001,\n",
    "    betas=(0.9, 0.999)\n",
    ")"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Summary of Optimisers\n",
    "\n",
    "| Class                             | Convergence speed | Convergence quality   |\n",
    "|-----------------------------------|-------------------|-----------------------|\n",
    "|`SGD`                              |     Bad           |         Good          |\n",
    "|`SGD(momentum=...)`                |     Average       |         Good          |\n",
    "|`SGD(momentum=..., nesterov=True)` |     Average       |         Good          |\n",
    "|`Adagrad`                          |     Good          | Bad (stops too early) |\n",
    "|`RMSprop`                          |     Good          |     Average/Good      |\n",
    "|`Adam`                             |     Good          |     Average/Good      |\n",
    "|`AdaMax`                           |     Good          |     Average/Good      |\n",
    "|`Nadam`                            |     Good          |     Average/Good      |"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. Learning Rate Scheduling\n",
    "\n",
    "The idea here is to start with a large learning rate and decrease it as the number of iterations over the training set increases."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exponential LR scheduling\n",
    "\n",
    "$$\\eta[t] = \\eta_{0} 0.1^{t/s}$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from torch.optim import lr_scheduler\n",
    "\n",
    "class MNISTClassifier(pl.LightningModule):\n",
    "    def __init__(self):\n",
    "        super().__init__()\n",
    "        ...\n",
    "\n",
    "    def forward(self, x: torch.Tensor) -> torch.Tensor:\n",
    "        \"\"\"\n",
    "        Forward pass for our Classifier\n",
    "        \"\"\"\n",
    "        ...\n",
    "        return x\n",
    "    \n",
    "    def training_step(self, batch, batch_idx):\n",
    "        ...\n",
    "\n",
    "    def configure_optimizers(self):\n",
    "        optimizer = torch.optim.Adam(self.parameters(), lr=1e-3)\n",
    "        scheduler = lr_scheduler.ExponentialLR(optimizer, gamma=0.1)\n",
    "        return [optimizer], [scheduler]"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Reduce LR on plateau\n",
    "\n",
    "Reduce learning rate when a metric has stopped improving. Models often benefit from reducing the learning rate by a factor of 2-10 once learning stagnates. This scheduler reads a metrics quantity and if no improvement is seen for a ‘patience’ number of epochs, the learning rate is reduced."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from torch.optim import lr_scheduler\n",
    "\n",
    "class MNISTClassifier(pl.LightningModule):\n",
    "    def __init__(self):\n",
    "        super().__init__()\n",
    "        ...\n",
    "\n",
    "    def forward(self, x: torch.Tensor) -> torch.Tensor:\n",
    "        \"\"\"\n",
    "        Forward pass for our Classifier\n",
    "        \"\"\"\n",
    "        ...\n",
    "        return x\n",
    "    \n",
    "    def training_step(self, batch, batch_idx):\n",
    "        ...\n",
    "\n",
    "    def configure_optimizers(self):\n",
    "        self.optimizer = torch.optim.Adam(self.parameters(), lr=1e-3)\n",
    "        self.scheduler = lr_scheduler.ReduceLROnPlateau(optimizer, mode=\"min\")\n",
    "        return [self.optimizer], [self.scheduler]\n",
    "    \n",
    "    def optimizer_step(self, epoch_nb, batch_nb, optimizer, optimizer_i, second_order_closure=None):\n",
    "        self.optimizer.step()\n",
    "        self.optimizer.zero_grad()\n",
    "        if self.trainer.global_step % self.config.val_check_interval == 0:\n",
    "            self.scheduler.step(self.val_loss)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 4. Regularization\n",
    "\n",
    "### 4.1 ℓ2 Regularization\n",
    "\n",
    "Just like we did for Linear Regression in Week 3, we can use ℓ2 (i.e. \"Ridge\") regularization  to constrain a neural network’s connection weights. In the cell below we apply ℓ2 regularization to a PyTorch optimiser, using a regularization factor of `1e-5`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "optimizer = torch.optim.Adam(model.parameters(), lr=1e-4, weight_decay=1e-5)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 4.2 Dropout\n",
    "\n",
    "Dropout has become one of the most popular regularization techniques for deep neural networks. Originally proposed by Geoffrey Hinton in 2012 it was later exposed in more detail in a paper by Nitish Srivastava et al. (2014). It is a very successful technique: state-of-the-art neural networks get a 1–2% accuracy improvement just by adding dropout. This means that when a network already has 95% accuracy, getting a 2% accuracy increase means reducing the error by 40% (from 5% to about 3%).\n",
    "\n",
    "The algorithm works like this: at each training step, each neuron (including input neurons, but excluding output neurons) has a probability p of being “dropped out” temporarily. This means that it will be completely ignored during the current training step, but it may be active during the following step. The hyperparameter _p_ is named dropout rate, and it usually falls between 10% and 50%: closer to 20–30% in recurrent neural nets (we will see some of these for NLP), and closer to 40–50% in convolutional neural networks (we will see these for Computer Vision mainly). After training, nthere is no more dropout.\n",
    "\n",
    "In practice, dropout is only applied to the neurons in the top 1-3 layers (still excluding the output layer). See the example below of a fully connected Network with dropout rate of 20%. As you see all you have to do is to add a `Dropout` layer:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dropout_50 = nn.Dropout(p=0.5) # 50% dropout\n",
    "out = dropout_50(selu(layer(x)))\n",
    "out"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dropout_20 = nn.Dropout(p=0.2) # 20% dropout\n",
    "out = dropout_20(selu(layer(x)))\n",
    "out"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.11"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
