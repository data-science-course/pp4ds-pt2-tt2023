import numpy as np
import lightning.pytorch as pl
import torch
import torchmetrics
import torchvision
from torch import nn, optim, utils

class SimpleMNISTClassifier(pl.LightningModule):
    def __init__(self):
        super().__init__()
        self.layer_1 = nn.Linear(28 * 28, 128)
        self.layer_2 = nn.Linear(128, 256)
        self.layer_3 = nn.Linear(256, 10)
        self.relu = nn.ReLU()
        self.softmax = nn.Softmax(dim=1)  # apply softmax to the input dimension

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        """
        Forward pass for our Classifier
        """
        batch_size, channels, height, width = x.size()
        # print(f"Batch size = {batch_size}, channels = {channels}, width = {width}, height = {height}")
        # flatten the input image across (channels, height, width)
        x = x.view(batch_size, -1)
        x = self.layer_1(x)
        x = self.relu(x)
        x = self.layer_2(x)
        x = self.relu(x)
        x = self.layer_3(x)
        x = self.softmax(x)
        return x


    def training_step(self, batch, batch_idx):
        x, y = batch
        # print(f"Batch idx #{batch_idx}. Input shape = {x.shape}; Output shape = {y.shape}")
        # batch_size, channels, height, width = x.size()
        # print(f"Batch size = {batch_size}, channels = {channels}, width = {width}, height = {height}")
        logits = self.forward(x)
        loss = nn.functional.cross_entropy(logits, y)
        self.log("train_loss", loss, on_step=True, on_epoch=True)
        return loss

    def validation_step(self, val_batch, batch_idx):
        x, y = val_batch
        val_logits = self.forward(x)
        val_loss = nn.functional.cross_entropy(val_logits, y)
        self.log("val_loss", val_loss, on_step=True, on_epoch=True)
        val_accuracy = torchmetrics.functional.accuracy(
            val_logits,
            y,
            task="multiclass",
            num_classes=10,
        )
        self.log("val_accuracy", val_accuracy, on_step=True, on_epoch=True)

    def test_step(self, test_batch, batch_idx):
        x, y = test_batch
        test_logits = self.forward(x)
        test_loss = nn.functional.cross_entropy(test_logits, y)
        self.log("val_loss", test_loss)
        test_accuracy = torchmetrics.functional.accuracy(
            test_logits,
            y,
            task="multiclass",
            num_classes=10,
        )
        self.log("test_accuracy", test_accuracy)

    def prediction_step(self, batch, batch_idx):
        return self(batch)

    def configure_optimizers(self):
        optimizer = optim.SGD(self.parameters(), lr=1e-2)
        return optimizer
    

class WideAndDeepMNISTClassifier(pl.LightningModule):
    def __init__(self):
        super().__init__()
        self.layer_1 = nn.Linear(28 * 28, 300)
        self.layer_2 = nn.Linear(300, 100)
        self.output = nn.Linear(400, 10)
        self.relu = nn.ReLU()
        self.softmax = nn.Softmax(dim=1)  # apply softmax to the input dimension

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        """
        Forward pass for our Classifier
        """
        batch_size, channels, height, width = x.size()
        # print(f"Batch size = {batch_size}, channels = {channels}, width = {width}, height = {height}")
        # flatten the input image across (channels, height, width)
        x = x.view(batch_size, -1)
        x = self.layer_1(x)
        x = self.relu(x)
        y = self.layer_2(x)
        y = self.relu(y)
        z = torch.cat((x, y), dim=1)
        z = self.output(z)
        z = self.softmax(z)
        return z


    def training_step(self, batch, batch_idx):
        x, y = batch
        # print(f"Batch idx #{batch_idx}. Input shape = {x.shape}; Output shape = {y.shape}")
        # batch_size, channels, height, width = x.size()
        # print(f"Batch size = {batch_size}, channels = {channels}, width = {width}, height = {height}")
        logits = self.forward(x)
        loss = nn.functional.cross_entropy(logits, y)
        self.log("train_loss", loss, on_step=True, on_epoch=True)
        return loss

    def validation_step(self, val_batch, batch_idx):
        x, y = val_batch
        val_logits = self.forward(x)
        val_loss = nn.functional.cross_entropy(val_logits, y)
        self.log("val_loss", val_loss, on_epoch=True)
        val_accuracy = torchmetrics.functional.accuracy(
            val_logits,
            y,
            task="multiclass",
            num_classes=10,
        )
        self.log("val_accuracy", val_accuracy, on_step=True, on_epoch=True)

    def test_step(self):
        pass

    def prediction_step(self):
        pass

    def configure_optimizers(self):
        optimizer = optim.Adam(self.parameters(), lr=1e-3)
        return optimizer