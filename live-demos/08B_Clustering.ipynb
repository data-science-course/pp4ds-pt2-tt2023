{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Week 8: Clustering\n",
    "\n",
    "(Heavy inspired by Aurelien Geron's notebook on Unsupervised Learning: see https://github.com/ageron/handson-ml/blob/master/08_dimensionality_reduction.ipynb)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The great majority of the applications of ML today are based on supervised learning. However, the vast majority of the available data is unlabeled: we have the input features **X**, but we do not have the labels **y**. \n",
    "\n",
    "“If intelligence was a cake, unsupervised learning would be the cake, supervised learning would be the icing on the cake, and reinforcement learning would be the cherry on the cake.” (Yann LeCun)\n",
    "\n",
    "There is a huge potential in unsupervised learning that haven't been explored at all yet. One of the main families of unsupervised learning algorithms is clustering."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Setup"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Python ≥3.9 is required\n",
    "import sys\n",
    "assert sys.version_info >= (3, 9)\n",
    "\n",
    "# Scikit-Learn ≥1.0 is required\n",
    "import sklearn\n",
    "assert sklearn.__version__ >= \"1.0\"\n",
    "\n",
    "# Common imports\n",
    "import numpy as np\n",
    "\n",
    "# to make this notebook's output stable across runs\n",
    "np.random.seed(42)\n",
    "\n",
    "# To plot pretty figures\n",
    "%matplotlib inline\n",
    "import matplotlib as mpl\n",
    "import matplotlib.pyplot as plt\n",
    "mpl.rc('axes', labelsize=14)\n",
    "mpl.rc('xtick', labelsize=12)\n",
    "mpl.rc('ytick', labelsize=12)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. K-means"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.1 Create some data\n",
    "\n",
    "We generate some blobs of data that we will use to perform clustering using `sklearn.datasets.make_blobs`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.datasets import make_blobs\n",
    "\n",
    "# centres of the blobs\n",
    "blob_centers = np.array(\n",
    "    [[ 0.5,  2.3],\n",
    "     [-1.7 ,  2.3],\n",
    "     [-3.0,  1.9],\n",
    "     [-3.0,  3.0],\n",
    "     [-3.0,  1.4]]\n",
    ")\n",
    "# standard deviations of the blobs\n",
    "blob_std = np.array([0.4, 0.3, 0.1, 0.1, 0.1])\n",
    "\n",
    "X, y = make_blobs(\n",
    "    n_samples=2000,\n",
    "    centers=blob_centers,\n",
    "    cluster_std=blob_std,\n",
    "    random_state=7\n",
    ")"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's plot these blobs on the 2-D plane"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot_clusters(X, y=None):\n",
    "    plt.scatter(\n",
    "        X[:, 0],\n",
    "        X[:, 1],\n",
    "        c=y,\n",
    "        s=1\n",
    "    )\n",
    "    plt.xlabel(\"$x_1$\", fontsize=14)\n",
    "    plt.ylabel(\"$x_2$\", fontsize=14, rotation=0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(figsize=(8, 4))\n",
    "plot_clusters(X)\n",
    "plt.show()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.2 Train and predict\n",
    "\n",
    "Let's train a K-Means clusterer on this dataset. It will try to find each blob's center and assign each instance to the closest blob. Initialize a scikit-learn `KMeans` object, assigning it to a variable named `kmeans`. Pass as an argument the opportune number of clusters. Then, train train the mode and make predictions on the training data, saving the predicted clusters for each data instance in `X` in a variable `y_pred`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.cluster import KMeans\n",
    "k = 5\n",
    "kmeans = KMeans(\n",
    "  n_clusters=k,\n",
    "  n_init=10, # this used to be the default value. The algorithm will be run 10 times to improve convergence.\n",
    "  random_state=42\n",
    ")\n",
    "y_pred = kmeans.fit_predict(X)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, each instance in the training data has been assigned to one of the 5 clusters:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Let's print out all the predictions (i.e. predicted clusters for all the data instances in X)\n",
    "y_pred"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "kmeans.labels_"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "y_pred is kmeans.labels_"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "kmeans.cluster_centers_"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can also predict on new data. Each new data instance is assigned to the clusted whose centroid is closer."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X_new = np.array([[0, 2], [3, 2], [-3, 3], [-3, 2.5]])\n",
    "kmeans.predict(X_new)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.1 Display the decision boundaries in 2D"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will use the `plot_decision_boundaries` declared in the `clustering_utils.py` module (stored in this same directory).\n",
    "\n",
    "Have a look at the `plot_decision_boundaries` function defined above and use it to plot the decision boundaries learned by `kmeans` over the training data `X`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from clustering_utils import plot_decision_boundaries\n",
    "\n",
    "plt.figure(figsize=(8, 4))\n",
    "plot_decision_boundaries(kmeans, X)\n",
    "plt.show()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that some of the instances near the edges have been assigned to the wrong cluster."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.2 K-means algorithm\n",
    "\n",
    "The K-Means algorithm is one of the fastest clustering algorithms, but also one of the simplest:\n",
    "* First initialize $k$ centroids randomly: $k$ distinct instances are chosen randomly from the dataset and the centroids are placed at their locations.\n",
    "* Repeat the following until convergence (i.e., until the centroids stop moving):\n",
    "    * Assign each instance to the closest centroid.\n",
    "    * Update the centroids to be the mean of the instances that are assigned to them.\n",
    "    \n",
    "Instead of initializing the centroids entirely randomly, it is preferable to initialize them using the following algorithm, proposed in a [2006 paper](https://goo.gl/eNUPw6) by David Arthur and Sergei Vassilvitskii:\n",
    "* Take one centroid $c_1$, chosen uniformly at random from the dataset.\n",
    "* Take a new center $c_i$, choosing an instance $\\mathbf{x}_i$ with probability: $D(\\mathbf{x}_i)^2$ / $\\sum\\limits_{j=1}^{m}{D(\\mathbf{x}_j)}^2$ where $D(\\mathbf{x}_i)$ is the distance between the instance $\\mathbf{x}_i$ and the closest centroid that was already chosen. This probability distribution ensures that instances that are further away from already chosen centroids are much more likely be selected as centroids.\n",
    "* Repeat the previous step until all $k$ centroids have been chosen.\n",
    "\n",
    "This approach of initializing centroids, named \"Kmeans++\" is now the default for scikit-learn's k-means."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "KMeans()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.3 Inertia\n",
    "\n",
    "To select the best model, we will need a way to evaluate a K-Mean model's performance. Unfortunately, clustering is an unsupervised task, so we do not have the targets. But at least we can measure the distance between each instance and its centroid. This is the idea behind the _inertia_ metric:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "kmeans.inertia_"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Inertia is just the sum of the squared distances between each training instance and its closest centroid.\n",
    "\n",
    "If we run the `transform()` method we get the distance of each instance to each of the 5 centroids:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X_dist = kmeans.transform(X)\n",
    "X_dist  # For each instance we have the distance to each centroid (5 centroids, 5 distance measures)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using the predicted labels we can get for each instance the distance to the closet centroid (i.e. the predicted cluster)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X_dist[np.arange(len(X_dist)), kmeans.labels_]"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we sum the squared values of all these distances we get the inertia for our clusterer."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.sum(X_dist[np.arange(len(X_dist)), kmeans.labels_]**2)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.4 Finding the optimal number of clusters\n",
    "\n",
    "So far we have cheated a bit, as we knew that our dataset was made up of 5 clusters. What if we did not know the number of clusters in advance?\n",
    "\n",
    "Let's try and see what happens if we train two K-means clusterers with 3 and 8 clusters and check their results.\n",
    "\n",
    "We will train them and plot the decision boundaries for both using the `plot_clusterer_comparison` function from the `clustering_utils.py` module. We will also check their inertia."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from clustering_utils import plot_clusterer_comparison\n",
    "\n",
    "kmeans_k3 = KMeans(n_clusters=3, n_init=10, random_state=42)\n",
    "kmeans_k8 = KMeans(n_clusters=8, n_init=10, random_state=42)\n",
    "\n",
    "plot_clusterer_comparison(kmeans_k3, kmeans_k8, X, \"$k=3$\", \"$k=8$\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "kmeans_k3.inertia_, kmeans_k8.inertia_"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So inertia keeps getting lower as we increase $k$. The more clusters there are, the closer each instance will be to its closest centroid, and therefore the lower the inertia will be. \n",
    "Inertia, _per se_ is not a good performance measure to determine the optimal $k$.\n",
    "However, we can plot the inertia as a function of $k$ and analyse the resulting curve.\n",
    "\n",
    "**Exercise:** train 9 K-Means clusterers, for $k$ from 1 to 9 included and save the inerias for each of them in a list of floats called `inertias`. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# complete this cell to solve the exercise. Hint: using list comprehension may help you.\n",
    "kmeans_per_k = ...\n",
    "inertias = ..."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you have solved correctly the exercise you can run the cell below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(figsize=(8, 3.5))\n",
    "plt.plot(range(1, 10), inertias, \"bo-\")\n",
    "plt.xlabel(\"$k$\", fontsize=14)\n",
    "plt.ylabel(\"Inertia\", fontsize=14)\n",
    "# add an arrow pointing to the elbow\n",
    "plt.annotate('Elbow',\n",
    "             xy=(4, inertias[3]),\n",
    "             xytext=(0.55, 0.55),\n",
    "             textcoords='figure fraction',\n",
    "             fontsize=16,\n",
    "             arrowprops=dict(facecolor='black', shrink=0.1)\n",
    "            )\n",
    "plt.axis([1, 8.5, 0, 1300])\n",
    "plt.show()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see, there is an elbow at $k=4$, which means that less clusters than that would be bad, and more clusters would not help much and might cut clusters in half. So $k=4$ is a pretty good choice. Of course in this example it is not perfect since it means that the two blobs in the lower left will be considered as just a single cluster, but it's a pretty good clustering nonetheless."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_decision_boundaries(kmeans_per_k[4-1], X)\n",
    "plt.show()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Another approach is to look at the _silhouette score_, which is the mean _silhouette coefficient_ over all the instances. \n",
    "\n",
    "An instance's silhouette coefficient is equal to $(b - a)/\\max(a, b)$ where: \n",
    "* $a$ is the mean distance to the other instances in the same cluster (it is the _mean intra-cluster distance_).\n",
    "* $b$ is the _mean nearest-cluster distance_, that is the mean distance to the instances of the next closest cluster.\n",
    "\n",
    "The silhouette coefficient can vary between -1 and +1: a coefficient close to +1 means that the instance is well inside its own cluster and far from other clusters, while a coefficient close to 0 means that it is close to a cluster boundary, and finally a coefficient close to -1 means that the instance may have been assigned to the wrong cluster.\n",
    "\n",
    "A simple way to evaluate silohuette coefficients is to use the `sklearn.metrics.silhouette_score` function that returns the mean silhouette coefficient of all samples.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.metrics import silhouette_score\n",
    "silhouette_score(X, kmeans.labels_)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the cell below we compute the silhouette score for all k values up to 10 and then plot the result:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "silhouette_scores = [\n",
    "  silhouette_score(X, model.labels_)\n",
    "  for model in kmeans_per_k[1:]\n",
    "]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(figsize=(8, 3))\n",
    "plt.plot(range(2, 10), silhouette_scores, \"bo-\")\n",
    "plt.xlabel(\"$k$\", fontsize=14)\n",
    "plt.ylabel(\"Silhouette score\", fontsize=14)\n",
    "plt.axis([1.8, 8.5, 0.55, 0.75])\n",
    "plt.show()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see, this visualization is much richer than the previous one: in particular, although it confirms that $k=4$ is a very good choice, but it also underlines the fact that $k=5$ is quite good as well.\n",
    "\n",
    "An even more informative visualization is given when you plot every instance's silhouette coefficient, sorted by the cluster they are assigned to and by the value of the coefficient. This is called a _silhouette diagram_."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise:** Have a look at the `plot_silouhette_diagrams()` function in `clustering_utils.py` and use it to plot the silouhette diagrams for k values from 3 to 6.\n",
    "\n",
    "Have a look at the graphs. What can you infer from it? We'll have a discussion in the class."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Write your solution here\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise**: The Olivetti faces dataset contains 400 64x64 gray-scale images of faces of the same 40 persons, each of them flattened to a 4,096 1D vector.\n",
    "  * Load the dataset (using `sklearn.datasets.fetch_olivetty_faces()`)\n",
    "  * split it into training, validation, and test set (you probably want to use a stratified split to ensure the same number of pictures per person end up in each set)\n",
    "  * check if the data is already scaled. If not scale it\n",
    "  * cluster it using K-means, and ensure you have a good number of clusters (verify that with elbow or silouhette technique)\n",
    "  * finally visualize the clusters and check if you see similar faces in each cluster "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.datasets import fetch_olivetti_faces\n",
    "oliv = fetch_olivetti_faces()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.model_selection import StratifiedShuffleSplit"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "splitter1 = StratifiedShuffleSplit(\n",
    "    n_splits=1, test_size=0.2, random_state=42\n",
    ")\n",
    "for train_val_index, test_index in splitter1.split(oliv.data, oliv.target):\n",
    "    train_val_set = oliv.data[train_val_index]\n",
    "    train_val_set_labels = oliv.target[train_val_index]\n",
    "    test_set = oliv.data[test_index]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "splitter2 = StratifiedShuffleSplit(\n",
    "    n_splits=1, test_size=0.25, random_state=42\n",
    ")\n",
    "for train_index, val_index in splitter2.split(train_val_set, train_val_set_labels):\n",
    "    train_set = train_val_set[train_index]\n",
    "    val_set = train_val_set[val_index]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "train_set.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "val_set.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "test_set.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "kmeans_per_k = [\n",
    "    KMeans(n_clusters=k, n_init=10, random_state=42).fit(train_set)\n",
    "    for k in range(10, 120, 5)\n",
    "]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "silhouette_scores = [\n",
    "  silhouette_score(train_set, model.labels_)\n",
    "  for model in kmeans_per_k\n",
    "]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(figsize=(8, 3))\n",
    "plt.plot(range(10, 120, 5), silhouette_scores, \"bo-\")\n",
    "plt.xlabel(\"$k$\", fontsize=14)\n",
    "plt.ylabel(\"Silhouette score\", fontsize=14)\n",
    "plt.show()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. DBSCAN for clustering (and anomaly detection)\n",
    "\n",
    "The DBSCAN algorithm defines clusters as continuous regions of high density. It works as follows:\n",
    "\n",
    "* For each instance on the training set, DBSCAN counts how many instances are located within a small distance ε (epsilon) from it. This region is called the instance’s ε-neighborhood.\n",
    "\n",
    "* If an instance has at least _m_ instances in its ε-neighborhood (including itself), then it is considered a core instance. This means that core instances are those instances located in dense regions.\n",
    "\n",
    "* All instances in the neighborhood of a core instance belong to a cluster. This neighbourhood may include other core instances; hence, a sequence of neighbouring core instances belongs to a single cluster.\n",
    "\n",
    "* Any instance that is not a core instance and does not have one in its neighborhood is treated as an _anomaly_. Hence, DBSCAN can be used for anomaly detection\n",
    "\n",
    "Let's generate a dataset using the `make_moons` function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.datasets import make_moons\n",
    "X, y = make_moons(n_samples=1000, noise=0.05, random_state=42)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "_ = plt.scatter(X[:, 0], X[:, 1])"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's create and train a DBSCAN clusterer"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.cluster import DBSCAN\n",
    "dbscan = DBSCAN(\n",
    "    eps=0.05,\n",
    "    min_samples=5\n",
    ")\n",
    "dbscan.fit(X)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Labels for all the instances can be found in the `labels_` attribute"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dbscan.labels_[:10]"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The core instances can be found in the `core_sample_indices_` attribute."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "len(dbscan.core_sample_indices_)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this case we have 808 core instances out of 1000. The indices of the core instances can be found in the `core_sample_indices_` attribute and the core instances themselves can be found in the `components_` attribute."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dbscan.core_sample_indices_[:10]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dbscan.components_[:3]"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can check the unique values for the labels."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.unique(dbscan.labels_)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We have 7 clusters (0 to 6), while -1 is used to label any instance that did not fall into a cluster.\n",
    "\n",
    "Let's train another DBSCAN clusterer with a different epsilon."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dbscan2 = DBSCAN(eps=0.2)\n",
    "dbscan2.fit(X)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from clustering_utils import plot_dbscan\n",
    "\n",
    "plt.figure(figsize=(9, 3.2))\n",
    "\n",
    "plt.subplot(121)\n",
    "plot_dbscan(dbscan, X, size=100)\n",
    "\n",
    "plt.subplot(122)\n",
    "plot_dbscan(dbscan2, X, size=600, show_ylabels=False)\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. Other Clustering Algorithms (not discussed)\n",
    "\n",
    "* Spectral Clustering\n",
    "* Agglomerative Clustering\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 4. Gaussian Mixture Models\n",
    "\n",
    "A Gaussian mixture model (GMM) is a probabilistic model that assumes that the dat instances were obtained from a weighted combination of a certain number of Gaussian distributions whose parameters (means and covariances) are unknown.\n",
    "\n",
    "All the instances generated by from a single Gaussian Distribution will form a cluster of ellipsoidal shape with different size and orientation. You have to learn out of which distribution each data instance was generated as well as all the parameters of the distribution itself.\n",
    "\n",
    "Parameters to be learned in this case are:\n",
    "* weight of each Gaussian\n",
    "* mean of each Gaussian\n",
    "* covariance matrices of each Gaussian ($N \\times N$ matrix, where $N$ is the number of dimensions in our input)\n",
    "\n",
    "Let's create a `GaussianMixture` model instance and train it on the moon dataset we used for the DBSCAN."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.mixture import GaussianMixture\n",
    "\n",
    "gm = GaussianMixture(\n",
    "  n_components=3,\n",
    "  n_init=10 # reinitialize the algorithm up to 10 times to improve convergence\n",
    ")\n",
    "gm.fit(X)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Like K-Means, the Expectation-Maximisation algorithm run by a GMM can end up converging to poor solutions. To overcome this, it needs to be run several times, keeping the best solution. This is why we have set n_init to 10. Warning: n_init has a default value of 1.\n",
    "\n",
    "We can check the parameters that our model has estimated:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gm.weights_"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gm.means_"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gm.covariances_"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also check whether the algorithm has converged and how many iterations it took to converge."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gm.converged_, gm.n_iter_"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Good, the algorithm has converged. Let's now make predictions. Gaussian Mixture models can be used for both hard clustering (same as K-means and DBSCAN) but also for soft clustering (They will return a probability that a particular instance belongs to each cluster)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Hard clustering\n",
    "y_pred = gm.predict(X)\n",
    "y_pred[:5]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Soft clustering\n",
    "y_pred = gm.predict_proba(X)\n",
    "y_pred[:5]"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now you can use `predict()` and `predict_proba()` for hard and soft clustering on new data instances. You can also use the model to generate new instances for it.\n",
    "\n",
    "**Exercise:** try and generate 20 new samples using the `gm` model we have created and trained above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Write your solution here"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.9"
  },
  "vscode": {
   "interpreter": {
    "hash": "8ffd1997cd8332673e5cfa75c134bb844dadbe437a664ff131aab49ac4940c69"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
