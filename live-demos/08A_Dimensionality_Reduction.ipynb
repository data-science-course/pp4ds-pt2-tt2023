{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Week 8: Dimensionality Reduction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Many Machine Learning problems involve thousands or even millions of features for each training instance.\n",
    "This has a few consequences:\n",
    "* training becomes too slow\n",
    "* it may be very difficult to find a good, let alone optimal, solution\n",
    "\n",
    "This problem is often called the _curse of dimensionality_.\n",
    "\n",
    "Fortunately, in real-world situations, it is possible to reduce the number of features. This makes problems tractable."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Setup"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Python ≥3.9 is required\n",
    "import sys\n",
    "assert sys.version_info >= (3, 9)\n",
    "\n",
    "# Scikit-Learn ≥1.0 is required\n",
    "import sklearn\n",
    "assert sklearn.__version__ >= \"1.0\"\n",
    "\n",
    "# Common imports\n",
    "import numpy as np\n",
    "import os\n",
    "\n",
    "# to make this notebook's output stable across runs\n",
    "np.random.seed(42)\n",
    "\n",
    "# To plot pretty figures\n",
    "import matplotlib as mpl\n",
    "import matplotlib.pyplot as plt\n",
    "mpl.rc('axes', labelsize=14)\n",
    "mpl.rc('xtick', labelsize=12)\n",
    "mpl.rc('ytick', labelsize=12)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Principal Component Analysis (PCA)\n",
    " \n",
    "This is by far the most popular dimensionality reduction algorithm. The goal of PCA is to project our dataset from high-dimensional hyperplane onto a lower-dimensional hyperplane.\n",
    "\n",
    "PCA identifies the axis that accounts for the largest amount of variance in the training set. It then identifies the other $n-1$ axes, orthogonal to the first and each other, sorted by how much variance they accoynt for. The $i^{th}$ axis is called the $i^{th}$ principal component (PC) of the data.\n",
    "\n",
    "The goal of PCA is to keep only those axis that significantly contribute to the variance in the data. The more the variance in the dataset the higher the information it contains.\n",
    "\n",
    "To show how PCA works, let's first build a 3-D example dataset whose number of dimensions we want to reduce. \n",
    "\n",
    "The dataset represents an ellipsis lying on a 2D plane within the 3D space with some noise added to it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.random.seed(4)\n",
    "m = 60  # number of samples\n",
    "w1, w2 = 0.1, 0.3\n",
    "noise = 0.1\n",
    "\n",
    "angles = np.random.rand(m) * 3 * np.pi / 2 - 0.5\n",
    "X = np.empty((m, 3))\n",
    "X[:, 0] = np.cos(angles) + np.sin(angles)/2 + noise * np.random.randn(m) / 2\n",
    "X[:, 1] = np.sin(angles) * 0.7 + noise * np.random.randn(m) / 2\n",
    "X[:, 2] = X[:, 0] * w1 + X[:, 1] * w2 + noise * np.random.randn(m)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X[:5]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from mpl_toolkits.mplot3d import Axes3D\n",
    "\n",
    "fig = plt.figure(figsize=(14, 10))\n",
    "ax = fig.add_subplot(111, projection='3d')\n",
    "\n",
    "# plot the dataset points\n",
    "ax.plot(X[:, 0], X[:, 1], X[:, 2], \"bo\", alpha=0.8)\n",
    "\n",
    "\n",
    "# add title and axis labels\n",
    "ax.set_title(\"3D Dataset\", fontsize=24)\n",
    "ax.set_xlabel(\"$x_1$\", fontsize=18, labelpad=10)\n",
    "ax.set_ylabel(\"$x_2$\", fontsize=18, labelpad=10)\n",
    "ax.set_zlabel(\"$x_3$\", fontsize=18, labelpad=10)\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's get the 2 main components using scikit-learn's PCA transformer."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.decomposition import PCA\n",
    "\n",
    "pca = PCA(n_components=2)\n",
    "X2D = pca.fit_transform(X)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X2D[:5]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can invert the transformation. However, keep in mind that there will be some information loss."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X3D_inv = pca.inverse_transform(X2D)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X3D_inv[:5]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X[:5]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `PCA` object gives access to the principal components that it computed:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pca.components_"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, let's have a look at how much each component contributes to the dataset variance."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pca.explained_variance_ratio_"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first dimension explains 84.2% of the variance, while the second explains 14.6%."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lost_variance_percentage = (1-sum(pca.explained_variance_ratio_))*100\n",
    "print(\n",
    "    f'By reducing the data to 2 dimensions we lost {lost_variance_percentage:.2f} % of the variance'\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## PCA: visualization"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from matplotlib.patches import FancyArrowPatch\n",
    "from mpl_toolkits.mplot3d import proj3d\n",
    "\n",
    "class Arrow3D(FancyArrowPatch):\n",
    "    \"\"\"\n",
    "    Utility class to draw a 3D arrow\n",
    "    \"\"\"\n",
    "    def __init__(self, xs, ys, zs, *args, **kwargs):\n",
    "        super().__init__((0,0), (0,0), *args, **kwargs)\n",
    "        self._verts3d = xs, ys, zs\n",
    "\n",
    "    def do_3d_projection(self, renderer=None):\n",
    "        xs3d, ys3d, zs3d = self._verts3d\n",
    "        xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, self.axes.M)\n",
    "        self.set_positions((xs[0],ys[0]),(xs[1],ys[1]))\n",
    "\n",
    "        return np.min(zs)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's first plot the 3D dataset, the plane and the projections on that plane:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create a 3D grid for our 3D plot\n",
    "axes = [-1.8, 1.8, -1.3, 1.3, -1.0, 1.0]\n",
    "\n",
    "x1s = np.linspace(axes[0], axes[1], 10)\n",
    "x2s = np.linspace(axes[2], axes[3], 10)\n",
    "x1, x2 = np.meshgrid(x1s, x2s)\n",
    "\n",
    "C = pca.components_\n",
    "R = C.T.dot(C)\n",
    "z = (R[0, 2] * x1 + R[1, 2] * x2) / (1 - R[2, 2])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from mpl_toolkits.mplot3d import Axes3D\n",
    "\n",
    "fig = plt.figure(figsize=(14, 10))\n",
    "ax = fig.add_subplot(111, projection='3d')\n",
    "\n",
    "# split the dataset into the items that lie above and below the 2D plane identified by PCA\n",
    "X3D_above = X[X[:, 2] > X3D_inv[:, 2]]\n",
    "X3D_below = X[X[:, 2] <= X3D_inv[:, 2]]\n",
    "\n",
    "# first plot the dataset points lying below the PC plane\n",
    "ax.plot(X3D_below[:, 0], X3D_below[:, 1], X3D_below[:, 2], \"bo\", alpha=0.5)\n",
    "\n",
    "# plot the 2D PC place onto the 3D space\n",
    "ax.plot_surface(x1, x2, z, alpha=0.2, color=\"k\")\n",
    "np.linalg.norm(C, axis=0)\n",
    "\n",
    "# Add 3D arrow describing the principal components\n",
    "ax.add_artist(Arrow3D([0, C[0, 0]],[0, C[0, 1]],[0, C[0, 2]], mutation_scale=15, lw=1, arrowstyle=\"-|>\", color=\"k\"))\n",
    "ax.add_artist(Arrow3D([0, C[1, 0]],[0, C[1, 1]],[0, C[1, 2]], mutation_scale=15, lw=1, arrowstyle=\"-|>\", color=\"k\"))\n",
    "ax.plot([0], [0], [0], \"k.\")\n",
    "\n",
    "# draw (black) lines highlighting the projections of the sample points onto the PC plane\n",
    "for i in range(m):\n",
    "    if X[i, 2] > X3D_inv[i, 2]:\n",
    "        ax.plot(\n",
    "            [X[i][0], X3D_inv[i][0]], [X[i][1], X3D_inv[i][1]], [X[i][2], X3D_inv[i][2]], \n",
    "            \"k-\"\n",
    "        )\n",
    "    else:\n",
    "        ax.plot(\n",
    "            [X[i][0], X3D_inv[i][0]], [X[i][1], X3D_inv[i][1]], [X[i][2], X3D_inv[i][2]],\n",
    "            \"-\",\n",
    "            color=\"#505050\"\n",
    "        )\n",
    "\n",
    "\n",
    "# plot the 3D samples as projected on the PC plane after PCA\n",
    "ax.plot(X3D_inv[:, 0], X3D_inv[:, 1], X3D_inv[:, 2], \"k+\")\n",
    "ax.plot(X3D_inv[:, 0], X3D_inv[:, 1], X3D_inv[:, 2], \"k.\")\n",
    "\n",
    "\n",
    "# now plot the dataset points lying above the PC plane\n",
    "ax.plot(X3D_above[:, 0], X3D_above[:, 1], X3D_above[:, 2], \"bo\")\n",
    "\n",
    "# add labels and plot limits\n",
    "ax.set_xlabel(\"$x_1$\", fontsize=18, labelpad=10)\n",
    "ax.set_ylabel(\"$x_2$\", fontsize=18, labelpad=10)\n",
    "ax.set_zlabel(\"$x_3$\", fontsize=18, labelpad=10)\n",
    "ax.set_xlim(axes[0:2])\n",
    "ax.set_ylim(axes[2:4])\n",
    "ax.set_zlim(axes[4:6])\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure()\n",
    "ax = fig.add_subplot(111, aspect='equal')\n",
    "\n",
    "ax.plot(X2D[:, 0], X2D[:, 1], \"k+\")\n",
    "ax.plot(X2D[:, 0], X2D[:, 1], \"k.\")\n",
    "ax.plot([0], [0], \"ko\")\n",
    "ax.arrow(0, 0, 0, 1, head_width=0.05, length_includes_head=True, head_length=0.1, fc='k', ec='k')\n",
    "ax.arrow(0, 0, 1, 0, head_width=0.05, length_includes_head=True, head_length=0.1, fc='k', ec='k')\n",
    "ax.set_xlabel(\"$z_1$\", fontsize=18)\n",
    "ax.set_ylabel(\"$z_2$\", fontsize=18, rotation=0)\n",
    "ax.axis([-1.5, 1.3, -1.2, 1.2])\n",
    "ax.grid(True)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### PCA: under the hood\n",
    "\n",
    "`scikit-learn`'s implementation of PCA uses Singular Value Decompositon (SVD) on the training set. SVD decomposes our $X$ features array into three components.\n",
    "\n",
    "$\\bf{X = U \\Sigma V^T}$\n",
    "\n",
    "The matrix $\\bf{V}$ contains the unit vectors $\\bf{c_i}$ that define our principal components\n",
    "\n",
    "$\\bf{V=(c_1, c_2, ..., c_n)}$\n",
    "\n",
    "$\\Sigma$ is a (rectangular) diagonal matrix containing the singular values associated to each principal components.\n",
    "\n",
    "$\\Sigma =  diag(\\sigma_1, \\sigma_2, ..., \\sigma_n)$ \n",
    "\n",
    "The biggest the absolute value of a singular value $\\mid{\\sigma_i}\\mid$ the more variance that principal components holds"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### PCA form data compression: MNIST\n",
    "\n",
    "We'll now use PCA to compress the MNIST dataset (e.g. as a data compression pre-processing step)\n",
    "\n",
    "Each image in the MNIST dataset is 28x28 in size, hence it has 784 features (dimensions). However, not all these features are equally informative, so it could be a reasonable candidate for PCA."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.datasets import fetch_openml\n",
    "\n",
    "mnist = fetch_openml('mnist_784', version=1, as_frame=False, parser=\"auto\")\n",
    "mnist.target = mnist.target.astype(np.uint8)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's split the dataset into training and test set."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.model_selection import train_test_split\n",
    "\n",
    "X = mnist.data\n",
    "y = mnist.target\n",
    "\n",
    "X_train, X_test, y_train, y_test = train_test_split(X, y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's first perform the PCA without reducing the dimensionality, and then find out the minimum number of dimensions necessary to preserve 95% of the training set variance."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pca = PCA()\n",
    "pca.fit(X_train)\n",
    "# compute the cumulative sum of the `explained_variance_ratio_` array\n",
    "cumsum = np.cumsum(pca.explained_variance_ratio_)\n",
    "# find the first index in the cumsum array whose value >= 0.95 (95%)\n",
    "n_dims = np.argmax(cumsum >= 0.95) + 1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n_dims"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So you need 154 dimensions out of the original 784 ($28 \\times 28$) to preserve 95% of the variance.\n",
    "\n",
    "You can also specify how much variance you want to preserve directly in the PCA constructor, using the argument `n_components`.\n",
    "\n",
    "**Exercise:** initialize a PCA reducer (named `pca`) that preserves 95% of the variance using `n_components`, train it and verify that it reduces the dimensions of `X_train` from 784 to 154."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# complete this cell to solve the e\n",
    "pca = ...\n",
    "X_reduced = ..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can now check how many features) i.e. pixels, would be enough to explain 95% of the variance."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pca.n_components_"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.sum(pca.explained_variance_ratio_)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can now reconstruct back the original 784-D images (with a 5% variance loss) from the compressed 154-D dataset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X_reconstructed = pca.inverse_transform(X_reduced)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's see how the reconstructed images compare to the original ones."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot_digits(instances, images_per_row=5, **options):\n",
    "    size = 28\n",
    "    images_per_row = min(len(instances), images_per_row)\n",
    "    images = [instance.reshape(size,size) for instance in instances]\n",
    "    n_rows = (len(instances) - 1) // images_per_row + 1\n",
    "    row_images = []\n",
    "    n_empty = n_rows * images_per_row - len(instances)\n",
    "    images.append(np.zeros((size, size * n_empty)))\n",
    "    for row in range(n_rows):\n",
    "        rimages = images[row * images_per_row : (row + 1) * images_per_row]\n",
    "        row_images.append(np.concatenate(rimages, axis=1))\n",
    "    image = np.concatenate(row_images, axis=0)\n",
    "    plt.imshow(image, cmap = mpl.cm.binary, **options)\n",
    "    plt.axis(\"off\")\n",
    "\n",
    "plt.figure(figsize=(12, 10))\n",
    "plt.subplot(121)\n",
    "plot_digits(X_train[::2100])\n",
    "plt.title(\"Original\", fontsize=16)\n",
    "plt.subplot(122)\n",
    "plot_digits(X_reconstructed[::2100])\n",
    "plt.title(\"Compressed\", fontsize=16)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see, even though there has been some information loss, the digits are still clearly recognizable.\n",
    "\n",
    "**Exercise:** perform the PCA transformation on the test set."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Write solution here.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Other dimensionality reduction techniques:\n",
    "\n",
    "* Manifold Learning\n",
    "* Autoencoders (we'll see an intro on these later in the course)\n",
    "\n",
    "### 2.1 Manifold learning\n",
    "\n",
    "*Manifold learning* is a class of unsupervised estimators that seeks to describe high-dimensional datasets as low-dimensional manifolds embedded in high-dimensional spaces. They are mostly used for visualization purposes.\n",
    "\n",
    "Manifold Learning can be thought of as an attempt to generalize linear frameworks like PCA to be sensitive to non-linear structure in data. Though supervised variants exist, the typical manifold learning problem is unsupervised: it learns the high-dimensional structure of the data from the data itself, without the use of predetermined classifications.\n",
    "\n",
    "PCA is a linear projection, which means it can’t capture non-linear dependencies. Manifold learning techniques are able to preserve the local structure of the data when projecting it to lower dimensions. A manifold is strictly defined a topological space that \"locally\" resembles Euclidean space, but this does not help us much. Some example of manifolds could help us understand it better. The surface of a sphere or a torus (i.e. a \"doughnut\") or a Moebius strip are examples of a 2D manifold embedded in a 3D space. Manifold learning aims at learning M-dimensional manifolds from an N-dimensional dataset where N > M (or even N >> M).\n",
    "\n",
    "There are a few widely used manifold learning algorithms:\n",
    "   * Isomap\n",
    "   * Locally Linear Embedding (LLE)\n",
    "   * T-distributed Stochastic Neighbor Embedding (t-SNE)\n",
    "   * Uniform Manifold Approximation and Projection (UMAP)\n",
    "\n",
    "The first three algorithms can be found on `scikit-learn`. We will now briefly see UMAP. You will have to install\n",
    "UMAP either using Anaconda Navigator or by running `!conda install -c conda-forge umap-learn` on a notebook cell (you can also run it on a terminal without the exclamation mark).\n",
    "\n",
    "UMAP is a dimension reduction technique that can be used for visualisation, but also for general non-linear dimension reduction.\n",
    "\n",
    "We'll split again the MNIST dataset into training and test set."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X, y = mnist.data, mnist.target\n",
    "\n",
    "# use slicing to create training and test set\n",
    "X_train, X_test, y_train, y_test = X[:60000], X[60000:], y[:60000], y[60000:]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.3 Manifold learning with t-SNE\n",
    "\n",
    "Let's create a t-SNE reducer and train it on our training set. We'll then transform our training set."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.manifold import TSNE\n",
    "\n",
    "reducer = TSNE(\n",
    "    n_components=2, # by default learn a 2D manifold\n",
    "    random_state=77\n",
    ")\n",
    "embedding = reducer.fit_transform(X_train)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's plot the results:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(14, 10))\n",
    "# I am taking all the space inside the figure\n",
    "ax = fig.add_axes([0, 0, 1, 1])\n",
    "\n",
    "scatter = ax.scatter(\n",
    "    embedding[:, 0], # first dimension of the resulting embedding\n",
    "    embedding[:, 1], # second dimension of the resulting embedding\n",
    "    c=y_train, # colour the dots by class (i.e. digit)\n",
    "    cmap='Spectral',\n",
    "    s=5 # size of the dots\n",
    ")\n",
    "ax.set_title('t-SNE 2D projection of the MNIST digits dataset', fontsize=24)\n",
    "fig.colorbar(scatter, boundaries=np.arange(11)-0.5).set_ticks(np.arange(10))\n",
    "fig.gca().set_aspect('equal', 'datalim')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.3 Manifold learning with UMAP\n",
    "\n",
    "Let's now create a UMAP reducer and train it on our training set. We'll then transform our training set."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import umap\n",
    "\n",
    "reducer = umap.UMAP(\n",
    "    n_components=2, # by default learn a 2D manifold\n",
    "    random_state=77\n",
    ")\n",
    "embedding = reducer.fit_transform(X_train)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's plot the results:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(14, 10))\n",
    "# I am taking all the space inside the figure\n",
    "ax = fig.add_axes([0, 0, 1, 1])\n",
    "\n",
    "scatter = ax.scatter(\n",
    "    embedding[:, 0], # first dimension of the resulting embedding\n",
    "    embedding[:, 1], # second dimension of the resulting embedding\n",
    "    c=y_train, # colour the dots by class (i.e. digit)\n",
    "    cmap='Spectral',\n",
    "    s=5 # size of the dots\n",
    ")\n",
    "ax.set_title('UMAP 2D projection of the MNIST digits dataset', fontsize=24)\n",
    "fig.colorbar(scatter, boundaries=np.arange(11)-0.5).set_ticks(np.arange(10))\n",
    "fig.gca().set_aspect('equal', 'datalim')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see the instances of our training set seem to cluster based on the actual class value. Notice that we haven't used the class for training, this is purely unsupervised learning."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise:** visualize the manifold embedding of the test set"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Write your solution here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise:** try a different manifold learning technique out of `scikit-learn` (e.g. t-SNE) and visualize the embeddings."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Write your solution here"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
