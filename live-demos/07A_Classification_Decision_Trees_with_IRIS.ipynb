{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Week 6: Classifications Tasks on the Iris Dataset\n",
    "\n",
    "## 1. Decision Trees\n",
    "\n",
    "Part of this Notebook was originally developed by [Jude Fletcher](https://eng.ox.ac.uk/people/jude-fletcher/).\n",
    "\n",
    "Decision Trees are a non-parametric supervised machine learning technique for classification and regression. Its aim is to create a model that predicts the value of a target variable by learning simple decision rules inferred from the data features or input vectors.\n",
    "\n",
    "<h3>Dataset: Iris </h3>\n",
    "\n",
    "The Iris flower data set or Fisher's Iris data set is a multivariate data set introduced by the British statistician and biologist Ronald Fisher in 1936. \n",
    "The data set consists of 50 samples from each of three species of Iris (Iris setosa, Iris virginica and Iris versicolor). Four features were measured from each sample: the length and the width of the sepals and petals, in centimeters. Based on the combination of these four features, Fisher developed a linear discriminant model to distinguish the species from each other.\n",
    "\n",
    "Source: https://en.wikipedia.org/wiki/Iris_flower_data_set\n",
    "\n",
    "<img src=\"../images/iris-data-set-01.png\" width=\"500\" style=\"float: left;\"> \n",
    "\n",
    "<img src=\"../images/iris-species.png\" width=\"500\" style=\"float: left;\"> \n",
    "\n",
    "\n",
    "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Setup"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Python ≥3.8 is required\n",
    "import sys\n",
    "assert sys.version_info >= (3, 8)\n",
    "\n",
    "# Scikit-Learn ≥1.0 is required\n",
    "import sklearn\n",
    "assert sklearn.__version__ >= \"1.0\"\n",
    "\n",
    "# Common imports\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import os\n",
    "\n",
    "# To plot pretty figures\n",
    "%matplotlib inline\n",
    "import matplotlib as mpl\n",
    "import matplotlib.pyplot as plt\n",
    "mpl.rc('axes', labelsize=14)\n",
    "mpl.rc('xtick', labelsize=12)\n",
    "mpl.rc('ytick', labelsize=12)\n",
    "\n",
    "# Precision options\n",
    "np.set_printoptions(precision=2)\n",
    "pd.options.display.float_format = '{:.3f}'.format"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h3>Loading and preparing the dataset.</h3>\n",
    "\n",
    "**NOTE**: decision trees don't need feature scaling."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.datasets import load_iris\n",
    "from sklearn.model_selection import train_test_split\n",
    "iris = load_iris()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "iris.keys()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X, y = iris.data, iris.target\n",
    "X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=77)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X_train[:3, :]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "y_train"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "iris[\"feature_names\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "iris[\"target_names\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Data Visualization and Exploration\n",
    "\n",
    "Let's have a very quick overview of the data plotting the scatter matrix"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "iris_df = pd.DataFrame(X_train, columns = iris.feature_names)\n",
    "iris_df['species'] = y_train\n",
    "iris_df['species'] = pd.Categorical(iris_df.species.apply(lambda y: iris.target_names[y]))\n",
    "iris_df.sample(10)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import seaborn as sns\n",
    "pal = sns.color_palette(n_colors=3)\n",
    "sns.pairplot(iris_df, hue='species')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise:** What can you infer from the graph?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Decision Tree: training and evaluation\n",
    "\n",
    "Now let's actually initialize and train our Decision Tree.\n",
    "\n",
    "<strong>DecisionTreeClassifier</strong> is a class capable of performing multi-class classification on a dataset - i.e. capable of both binary (where the labels are [-1, 1]) classification and multiclass (where the labels are [0, …, K-1]) classification.\n",
    "\n",
    "As with other classifiers, <strong>DecisionTreeClassifier.fit()</strong> takes as input two arrays:\n",
    "1. An array X, sparse or dense, of size <i>[n_samples, n_features]</i> holding the training samples<br>\n",
    "2. And an array Y of integer values, size <i>[n_samples]</i>, holding the class labels for the training samples"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.tree import DecisionTreeClassifier\n",
    "tree_cl = DecisionTreeClassifier()\n",
    "tree_cl.fit(X_train, y_train)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using the `plot_tree()` function we can plot the resulting decision tree."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.tree import plot_tree\n",
    "fig, ax = plt.subplots(figsize=(18, 12))\n",
    "res = plot_tree(\n",
    "    tree_cl, \n",
    "    feature_names=iris.feature_names, \n",
    "    class_names=iris.target_names,\n",
    "    rounded=True, \n",
    "    filled=True, \n",
    "    ax=ax,\n",
    "    fontsize=12\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Training a Decision Tree\n",
    "\n",
    "Given training array $\\mathbf{X}$ a label vector $\\mathbf{y}$, a decision tree recursively partitions the feature space such that the samples with the same labels or similar target values are grouped together. In order to do this, the training algorithm (called *CART*) aims at minimizing an inpurity metric, called \"gini\". The CART algorithm is a greedy algorithm, it finds the split that minimizes the gini value at the current level, it does not check if this choice will lead to the minimum gini value at the next iterations. In other words, it most often leads to a sub-optimal solution.\n",
    "\n",
    "What is the \"gini\" metric? This is the impurity index of that branching point. It is zero if all the items in the current subset are assigned to a class, greater than zero otherwise. The greater the gini value, the more spread are the values across multiple classes. \n",
    "\n",
    "$$ gini = \\sum_{j} p_j (1 - p_j) $$ \n",
    "\n",
    "An analogue metric is entropy often used in decision trees is entropy (aka the \"log loss\"), which is as well a measure of how spread are the values in multiple classes.\n",
    "\n",
    "$$ H =  - \\sum_{j} p_j \\log{p_j} $$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 1**: Evaluate your decision tree on the test set. Which performance metric would be suitable in this scenario?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Write your solution here\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 2**: Train a decision tree classifier on the iris dataset using entropy criterion and plot the resulting decision tree.\n",
    "\n",
    "Is this model performing better than the one trained using the gini impurity number?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Write your solution here\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Decision surfaces for Decision Trees\n",
    "\n",
    "Using the code below,we can plot the decision surface of a decision tree trained on pairs of features of the iris dataset. \n",
    "\n",
    "For each pair of iris features, the decision tree learns decision boundaries made of combinations of simple thresholding rules inferred from the training samples.\n",
    "\n",
    "NOTE: we are using the whole dataset as training set in this particular example, just for illustration purposes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Source code here: https://scikit-learn.org/stable/auto_examples/tree/plot_iris_dtc.html#sphx-glr-auto-examples-tree-plot-iris-dtc-py\n",
    "\n",
    "# Parameters\n",
    "n_classes = 3\n",
    "plot_colors = \"ryb\"\n",
    "plot_step = 0.02\n",
    "\n",
    "fig, ax = plt.subplots(2, 3, figsize=(12, 8))\n",
    "fig\n",
    "\n",
    "for pairidx, pair in enumerate([[0, 1], [0, 2], [0, 3],\n",
    "                                [1, 2], [1, 3], [2, 3]]):\n",
    "    # We only take the two corresponding features\n",
    "    X = iris.data[:, pair]\n",
    "    y = iris.target\n",
    "\n",
    "    # Train\n",
    "    clf = DecisionTreeClassifier().fit(X, y)\n",
    "\n",
    "    # Plot the decision boundary\n",
    "    plt.subplot(2, 3, pairidx + 1)\n",
    "\n",
    "    x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1\n",
    "    y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1\n",
    "    xx, yy = np.meshgrid(np.arange(x_min, x_max, plot_step),\n",
    "                         np.arange(y_min, y_max, plot_step))\n",
    "    plt.tight_layout(h_pad=0.5, w_pad=0.5, pad=2.5)\n",
    "\n",
    "    Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])\n",
    "    Z = Z.reshape(xx.shape)\n",
    "    cs = plt.contourf(xx, yy, Z, cmap=plt.cm.RdYlBu)\n",
    "\n",
    "    plt.xlabel(iris.feature_names[pair[0]])\n",
    "    plt.ylabel(iris.feature_names[pair[1]])\n",
    "\n",
    "    # Plot the training points\n",
    "    for i, color in zip(range(n_classes), plot_colors):\n",
    "        idx = np.where(y == i)\n",
    "        plt.scatter(X[idx, 0], X[idx, 1], c=color, label=iris.target_names[i],\n",
    "                    cmap=plt.cm.RdYlBu, edgecolor='black', s=15)\n",
    "\n",
    "plt.suptitle(\"Decision surface of a decision tree using paired features\", size=16)\n",
    "plt.legend(loc='lower right', borderpad=0, handletextpad=0)\n",
    "plt.axis(\"tight\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Final considerations on Decision Trees\n",
    "\n",
    "Decision Trees are *non-parametric models*. This means that the number of parameters is not pre-determined before training. This increases the risk that the model will overfit the training data (especially if you have a large number of features).\n",
    "\n",
    "To regularize the Decision Tree you can control the following parameters:\n",
    "* `max_depth`\n",
    "* `min_samples_split` (minimum number of samples that a node must have before being split)\n",
    "* `min_samples_leaf` (minimum number of samples a leaf node must have)\n",
    "* `min_weight_fraction_leaf` (same as `min_samples_leaf` but expressed a fraction of the total number of weighted instances)\n",
    "* `max_leaf_nodes` (maximum number of leaf nodes you want to allow)\n",
    "* `max_features` (maximum number of features that are evaluated for splitting at each node)\n",
    "\n",
    "Decision Trees are very easy to interpret. They are indeed a so-called explicitly interpretable ML model. However, they are very sensitive to variations in the training data and prone to overfitting. To avoid these drawbacks you can train an ensembe model built on a number of Decision Trees, a so called Random Forest.\n",
    "\n",
    "In the next section we will see Ensembe Models and Random Forests"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
