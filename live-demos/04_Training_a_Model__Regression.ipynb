{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Week 4: Training a model - Regression"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Python ≥3.9 is required\n",
    "import sys\n",
    "assert sys.version_info >= (3, 9)\n",
    "\n",
    "# Scikit-Learn ≥1.0 is required\n",
    "import sklearn\n",
    "assert sklearn.__version__ >= \"1.0\"\n",
    "\n",
    "# Common imports\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import os\n",
    "\n",
    "# To plot pretty figures\n",
    "%matplotlib inline\n",
    "import matplotlib as mpl\n",
    "import matplotlib.pyplot as plt\n",
    "mpl.rc('axes', labelsize=14)\n",
    "mpl.rc('xtick', labelsize=12)\n",
    "mpl.rc('ytick', labelsize=12)\n",
    "\n",
    "# Precision options\n",
    "np.set_printoptions(precision=2)\n",
    "pd.options.display.float_format = '{:.3f}'.format\n",
    "\n",
    "# Ignore useless warnings (see SciPy issue #5998)\n",
    "import warnings\n",
    "warnings.filterwarnings(action=\"ignore\", message=\"^internal gelsd\")\n",
    "# ignore convergence warnings for the purpose of this class\n",
    "warnings.filterwarnings(action=\"ignore\", category=sklearn.exceptions.ConvergenceWarning)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Load the data\n",
    "\n",
    "Let's load the data have we pre-processed in the previous notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "train_set_prepared = pd.read_csv(\"../datasets/prepared/kd-housing-train.csv\")\n",
    "test_set_prepared = pd.read_csv(\"../datasets/prepared/kd-housing-test.csv\")\n",
    "\n",
    "X_train = train_set_prepared.drop(columns=[\"price\"])\n",
    "y_train = train_set_prepared[\"price\"]\n",
    "\n",
    "X_test = test_set_prepared.drop(columns=[\"price\"])\n",
    "y_test = test_set_prepared[\"price\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X_train.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X_test.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X_train.info()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Linear Regression models"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will start by looking at the Linear Regression model, the simplest Regression model there is. There are two different ways to train it:\n",
    "\n",
    "* Using a direct “closed-form” equation that directly computes the model parameters that best fit the model to the training set (i.e., the model parameters that minimize the cost function over the training set).\n",
    "\n",
    "* Using an iterative optimization approach called Gradient Descent (GD) that gradually tweaks the model parameters to minimize the cost function over the training set, eventually converging to the same set of parameters as the first method. We will look at a few variants of Gradient Descent: Batch GD, Mini-batch GD, and Stochastic GD. This will be used again later on, when we will be seeing Neural Networks"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.1 Closed form solution: Normal Equation (Ordinary Least Squares)\n",
    "\n",
    "We will use the `LinearRegression` class from `scikit-learn`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.linear_model import LinearRegression\n",
    "lin_reg = LinearRegression()\n",
    "lin_reg.fit(X_train, y_train)\n",
    "lin_reg.intercept_, lin_reg.coef_"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "len(lin_reg.coef_)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let’s try it out on a few instances from the training set:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "some_data = X_train.iloc[:10]\n",
    "some_labels = y_train.iloc[:10]\n",
    "print(\"Predictions:\", lin_reg.predict(some_data))\n",
    "print(\"Labels:\", list(some_labels))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It mostly works, although the predictions are not exactly accurate. Let’s measure this regression model’s RMSE on the whole training set using Scikit-Learn’s `mean_squared_error()` function and computing its square root:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.metrics import mean_squared_error\n",
    "y_pred_lr = lin_reg.predict(X_train)\n",
    "lin_mse = mean_squared_error(y_train, y_pred_lr)\n",
    "lin_mse"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lin_rmse = np.sqrt(lin_mse)\n",
    "lin_rmse"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.2 Linear Regression using Gradient Descent\n",
    "\n",
    "Gradient Descent is a generic optimization algorithm capable of finding optimal solutions to a wide range of problems. The concept behind Gradient Descent is to modify parameters iteratively to minimize a cost function.\n",
    "\n",
    "The MSE cost function for a Linear Regression model is a convex function, which means that if you pick any two points on the curve, the line segment joining them never crosses the curve. This implies that there are no local minima, just one global minimum. It is also a continuous function with a slope that never changes abruptly."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 2.2.1 Batch Gradient Descent\n",
    "\n",
    "No example on this as on large datasets its not really feasible."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 2.2.2 Stochastic Gradient Descent\n",
    "\n",
    "To perform Linear Regression using Stochastic GD with Scikit-Learn, you can use the `SGDRegressor` class. It defaults to optimizing the squared error cost function. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.linear_model import SGDRegressor\n",
    "sgd_reg = SGDRegressor(\n",
    "    loss=\"squared_error\", # default cost function (MSE)\n",
    "    max_iter=2000,   # max numer of epochs. epoch = 1 full iteration over the training set\n",
    "    penalty=None,\n",
    "    eta0=1e-3,  # initial learning rate\n",
    "    tol=1e-3,   # stopping criterion tolerance. stop searching for a minimum \n",
    "                # (or maximum) once some tolerance is achieved, i.e. \n",
    "                # once you're close enough.\n",
    "    random_state=77\n",
    ")\n",
    "\n",
    "sgd_reg.fit(X_train, y_train)\n",
    "print(f\"SGD Regressor intercept: {sgd_reg.intercept_})\")\n",
    "print(f\"SGD Regressor coefficient: {sgd_reg.coef_}\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Predictions:\", sgd_reg.predict(some_data))\n",
    "print(\"Labels:\", list(some_labels))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "y_pred_sgd = sgd_reg.predict(X_train)\n",
    "sgd_mse = mean_squared_error(y_pred_sgd, y_train)\n",
    "sgd_rmse = np.sqrt(sgd_mse)\n",
    "sgd_rmse"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.3 Learning Curves\n",
    "\n",
    "Learning curves in scikit-learn can be used to assess how models will perform with varying numbers of training samples. This is achieved by monitoring the training and validation scores (e.g. RMSE) with an increasing number of training samples"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.model_selection import LearningCurveDisplay\n",
    "\n",
    "# create an SGD regressor object\n",
    "sgd_reg = SGDRegressor(\n",
    "    loss=\"squared_error\",\n",
    "    max_iter=2000,\n",
    "    penalty=None,\n",
    "    eta0=0.0001,\n",
    "    tol=1e-3,\n",
    ")\n",
    "\n",
    "# create an empty figure with an axis system\n",
    "fig, ax = plt.subplots(figsize=(12, 8))\n",
    "\n",
    "# generate the learning curves using the SGD regressor created above\n",
    "LearningCurveDisplay.from_estimator(\n",
    "    sgd_reg,\n",
    "    X_train,\n",
    "    y_train,\n",
    "    # see possible scoring parameters:\n",
    "    # https://scikit-learn.org/stable/modules/model_evaluation.html#scoring-parameter\n",
    "    scoring=\"neg_root_mean_squared_error\", #  negated RMSE \n",
    "    score_type=\"both\", # show both training and validation RMSE\n",
    "    negate_score=True, # need to negate as our scoring is \"negated\"\n",
    "    score_name=\"RMSE ($)\",\n",
    "    n_jobs=-1,  # use all the CPU processors\n",
    "    ax=ax # figure axis where the curves will be drawn\n",
    ")\n",
    "\n",
    "# personalize legend and figure title\n",
    "handles, label = ax.get_legend_handles_labels()\n",
    "ax.legend(handles[:2], [\"Training Score\", \"Validation Score\"])\n",
    "ax.set_title(f\"Learning Curve for {sgd_reg.__class__.__name__}\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What can you infer from the graph? Does the SGD regressor overfits?\n",
    "\n",
    "**Exercise:** draw the Learning curves without showing the standard deviation of the RMSE (i.e. without shaded area)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# write your solution here or edit the cell above\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise (at home):** draw the Learning curves using the $R^2$ score as evaluation metric"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# write your solution here\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.3 Early stopping\n",
    "\n",
    "Early stopping technique stops the training phase when the performance (e.g. the loss function) does not improve on a validation set after a certain number of epochs.\n",
    "\n",
    "**Exercise:** Here below there is an example of Earlt Stopping implementation. Please complete the missing lines "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.model_selection import train_test_split\n",
    "from sklearn.base import clone\n",
    "\n",
    "# create an SGD regressor object\n",
    "sgd_reg = SGDRegressor(\n",
    "    loss=\"squared_error\",\n",
    "    max_iter=1, # train only for one epoch at the time\n",
    "    penalty=None,\n",
    "    eta0=1e-4,\n",
    "    tol=1,\n",
    "    warm_start=True,\n",
    "    learning_rate=\"constant\" # the learning rate remains constant throughout training\n",
    ")\n",
    "\n",
    "min_val_error: float = float(\"inf\") # positive infinity\n",
    "best_epoch: int | None = None\n",
    "best_model: SGDRegressor | None = None\n",
    "\n",
    "n_epochs = 2000\n",
    "\n",
    "train_errors = np.zeros(n_epochs)\n",
    "val_errors = np.zeros(n_epochs)\n",
    "\n",
    "# partition the training set into a smaller training and validation set. Assign 25% of the samples to validation\n",
    "X_train_es, X_val_es, y_train_es, y_val_es = train_test_split(\n",
    "    X_train, y_train, test_size=0.25, random_state=42\n",
    ")\n",
    "\n",
    "for epoch in range(2000):\n",
    "    sgd_reg.fit(X_train_es, y_train_es) # as \"warm_start\" is true it will keep on training at each iteration - online learning\n",
    "    # make prediction on the training set\n",
    "    y_train_pred = ...\n",
    "    # make predictions on the validation set\n",
    "    y_val_pred = ...\n",
    "    # compute the MSE on the training set\n",
    "    train_error = ...\n",
    "    # compute the MSE on the validation set\n",
    "    val_error = ...\n",
    "    # check if the the current validation error is smaller than the minimum validation error\n",
    "    # if so update minimum validation error, best epoch and best model\n",
    "    ...\n",
    "    ...\n",
    "    ...\n",
    "    ...\n",
    "    # set train and validation error for the current epoch. They will be used later on to draw the plots\n",
    "    train_errors[epoch] = train_error\n",
    "    val_errors[epoch] = val_error\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's plot the training and validation errors (also called losses):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(train_errors, label=\"Training loss\")\n",
    "plt.plot(val_errors, label=\"Validation loss\")\n",
    "plt.xlabel(\"RMSE\")\n",
    "plt.ylabel(\"epoch\")\n",
    "plt.legend()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There is a clear plateau in the validation loss. Let's see which was the best epoch and the best model:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "best_epoch"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "best_model"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3. Polynomial Regression\n",
    "\n",
    "What if your data is more complex than a straight line (or an (N-1)-dimensional plane in an N-dimensional space)? Surprisingly, you can use a linear model to fit nonlinear data. This can be done with Polynomial Regression.\n",
    "\n",
    "Polynomial regression relies on polynomial features, which can get features’ high-order and interaction terms. \n",
    "\n",
    "![poly-reg](../images/poly_best_fits.png)\n",
    "\n",
    "For instance if you have the features $X$ $Y$, polynomial features up to degree 2 will be $1$, $X$, $Y$, $X^2$, $XY$ $Y^2$.\n",
    "\n",
    "Polynomial features up to degree 3 will be $1$, $X$, $Y$, $X^2$, $XY$ $Y^2$, $X^3$, $X^2Y$, $XY^2$, $Y^3$.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.preprocessing import PolynomialFeatures\n",
    "poly_features = PolynomialFeatures(degree=2, include_bias=False)\n",
    "X_train_poly = poly_features.fit_transform(X_train)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X_train_poly.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lin_reg = LinearRegression()\n",
    "lin_reg.fit(X_train_poly, y_train)\n",
    "\n",
    "lin_reg.intercept_, lin_reg.coef_"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "y_pred_poly = lin_reg.predict(\n",
    "    poly_features.transform(X_train)\n",
    ")\n",
    "poly_mse = mean_squared_error(\n",
    "    y_train, y_pred_poly\n",
    ")\n",
    "poly_rmse = np.sqrt(poly_mse)\n",
    "poly_rmse"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Our RMSE is now ~130 k\\\\$. This would seem an improvement over the pure linear regression RMSE. However, there is still a major issue. We are evaluating the RMSE with respect to the training set, that is the same data that we have used to train the model. If there were an overfitting issue we would not see it by computing the RMSE on the training set only.\n",
    "Adding many polynomial features can increase the complexity of your model, which can cause overfitting.\n",
    "\n",
    "To see if our Polynomial Regressor overfits we can train it in cross-validation\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 4. Model evaluation using Cross Validation\n",
    "\n",
    "Let's try and use the function `sklearn.model_selection.cross_val_score()` to evaluate our Polynomial Regression model on the training set using a 10-fold cross-validation with RMSE as a score. Print out the mean value and the standard deviation of the RMSE across the 10 validation iterations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "from sklearn.model_selection import cross_val_score\n",
    "scores = cross_val_score(\n",
    "    lin_reg, # model we want to train\n",
    "    X_train_poly, # features\n",
    "    y_train, # labels\n",
    "    scoring='neg_root_mean_squared_error', # this is the negated MSE\n",
    "    cv=10,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scores"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# You can also use this function to print out mean value and std of the scores\n",
    "def display_scores(scores):\n",
    "    print(\"Scores:\", scores)\n",
    "    print(\"Mean:\", f\"{scores.mean():.2f}\")\n",
    "    print(\"Standard deviation:\", f\"{scores.std():.2f}\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "poly_rmse_scores = -scores\n",
    "display_scores(poly_rmse_scores) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we can see the average RMSE in cross validation for the Polynomial Regressor is ~137 k$. While slightly worse than the RMSE on the training data is still not a considerable degradation, and it would seem to be still better than the performance of the Linear Regressor. We can conclude that the Polynomial Regressor is not significantly affected by overfitting.\n",
    "\n",
    "If I want to try more scoring metrics at once while performing cross-validation, and I want to get more information than just the scores themselves I can use `sklearn.model_selection.cross_validate()`.\n",
    "\n",
    "It would be good to check the performance of the Linear Regressor in cross-validation as well. Let's do that using `cross_validate()` and evaluating the $R_2$ metric alongside the RMSE."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.model_selection import cross_validate, KFold\n",
    "n_splits = 10\n",
    "k_fold = KFold(n_splits=n_splits, shuffle=True, random_state=42)\n",
    "cv_res = cross_validate(\n",
    "    lin_reg,\n",
    "    X_train,\n",
    "    y_train,\n",
    "    scoring=['neg_root_mean_squared_error', 'r2'],\n",
    "    cv=k_fold\n",
    ")\n",
    "cv_res"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lin_reg_rmse_scores = -cv_res['test_neg_root_mean_squared_error']\n",
    "display_scores(lin_reg_rmse_scores) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The RMSE in cross-validation for the linear regressor is 168k $. That value is basically the same as the RMSE for the training set. This is a good indicator that the Linear Regressor does not overfits at all. \n",
    "\n",
    "Still we have confirmed that the performance of the Polynomial Regressor is better at least in cross validation. We would still have to verify this on the test set, but we will keep that for the very last. We must use the test set only on the final chosen model."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Exercise:</b> Use the function `sklearn.model_selection.cross_val_score()` to evaluate our Stochastic Gradient Regressor model on the training set using a 5-fold cross-validation using the mean absolute error (MAE) as a score. Print out the mean value and the standard deviation of the mean absolute error across the 5 validation iterations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Write your solution here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 5. Regularised Linear Models\n",
    "\n",
    "### 5.1 Ridge Regression - $\\ell_2$ regularisation\n",
    "\n",
    "Ridge Regression is a regularized version of the Linear Regression algorithm: we add to the cost function a regularization term. This term for Ridge is equal to $ \\alpha \\sum_{i=1}^{N}{\\theta_i^2} $. This means that it forces the learning algorithm to fit not only the data but also keep the model weights as small as possible. An important thing to note that the regularization term should only be added to the cost function during training. Once the model is trained, you want to use the unregularized performance measure to evaluate the model’s performance.\n",
    "\n",
    "Ridge regression is a type of $\\ell_2$ regularisation, because the regularisation term is equal to half the square of the $\\ell_2$ norm of the weight vector."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.linear_model import Ridge\n",
    "ridge_reg = Ridge(alpha=25, solver=\"cholesky\")\n",
    "cv_res = cross_validate(\n",
    "    ridge_reg,\n",
    "    X_train_poly,\n",
    "    y_train,\n",
    "    scoring=['neg_root_mean_squared_error', 'r2'],\n",
    "    cv=k_fold\n",
    ")\n",
    "cv_res"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ridge_rmse_scores = -cv_res['test_neg_root_mean_squared_error']\n",
    "display_scores(ridge_rmse_scores) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Our 10-fold CV RMSE is now ~135.6k $, which is marginally better than the unregularized Polynomial Regression. We should also notice that regularisation has decreased the standard deviation of our metric across the various folds.\n",
    "\n",
    "At the moment, this is our best-performing model."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**NOTE**: If we create an `SGDRegressor` with \"squared_error\" loss and non-null penalty (default penalty is \"l2\") it will approximate a Ridge Regressor.\n",
    "\n",
    "**Exercise (at home):** Train an `SGDRegressor` with \"l2\" penalty and early stopping for 2000 epochs on `X_train_poly` and `y_train`. You can use the previous early stopping example as a source of inspiration."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# write your solution here\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 5.2  Lasso Regression - $\\ell_1$ regularisation\n",
    "\n",
    "Lasso Regression is a regularized version of the Linear Regression algorithm: we add to the cost function a regularization term. This term for Lasso is equal to $ \\alpha \\sum_{i=1}^{N}{\\mid \\theta_i \\mid} $.\n",
    "\n",
    "\n",
    "Lasso regression is a type of $\\ell_1$ regularisation, because the regularisation term is equal to the $\\ell_1$ norm of the weight vector.\n",
    "\n",
    "While $\\ell_2$ regularisation guarantees that all the weights are kept uniformly small, $\\ell_1$ regularisation tends to eliminate the weights of the least important features. The result of Lasso regularisation is a sparse model.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise:** train a `Lasso` regressor in cross validation and evaluate the average RMSE. Check the scikit-learn documentation and try a few values of $\\alpha$. How does it compare to the Ridge regressor?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Write you solution here:\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 5.3 ElasticNet\n",
    "\n",
    "ElasticNet combines $\\ell_1$ and $\\ell_2$ regularisation.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.linear_model import ElasticNet\n",
    "el_net = ElasticNet(alpha=25, l1_ratio=0.1, max_iter=10000)\n",
    "cv_res = cross_validate(\n",
    "    el_net,\n",
    "    X_train_poly,\n",
    "    y_train,\n",
    "    scoring=['neg_root_mean_squared_error', 'r2'],\n",
    "    cv=k_fold\n",
    ")\n",
    "cv_res"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "elnet_rmse_scores = -cv_res['test_neg_root_mean_squared_error']\n",
    "display_scores(elnet_rmse_scores) "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
