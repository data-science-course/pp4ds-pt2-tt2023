{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Multiclass and Multilabel Classification\n",
    "\n",
    "This notebook is heavily inspired by Andre Guernon work, that can be found here: https://github.com/ageron/handson-ml/blob/master/04_training_linear_models.ipynb"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Setup"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Python ≥ 3.9 is required\n",
    "import sys\n",
    "assert sys.version_info >= (3, 9)\n",
    "\n",
    "# Scikit-Learn ≥1.0 is required\n",
    "import sklearn\n",
    "assert sklearn.__version__ >= \"1.0\"\n",
    "\n",
    "# Common imports\n",
    "import numpy as np\n",
    "\n",
    "\n",
    "# To plot pretty figures\n",
    "%matplotlib inline\n",
    "import matplotlib as mpl\n",
    "import matplotlib.pyplot as plt\n",
    "mpl.rc('axes', labelsize=14)\n",
    "mpl.rc('xtick', labelsize=12)\n",
    "mpl.rc('ytick', labelsize=12)\n",
    "\n",
    "# Ignore useless warnings (see SciPy issue #5998)\n",
    "import warnings\n",
    "warnings.filterwarnings(action=\"ignore\", message=\"^internal gelsd\")"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The MNIST Dataset"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will be using the MNIST dataset, which is a set of 70,000 small images of digits handwritten by high school students and employees of the US Census Bureau. Each image is labeled with the digit it represents.\n",
    "\n",
    "We will use `sklearn.datasets.fetch_openml()` to fetch dataset from openml by name or dataset id."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.datasets import fetch_openml\n",
    "mnist = fetch_openml(\n",
    "    'mnist_784',\n",
    "    version=1,\n",
    "    as_frame=False, # we want the dataset as NumPy ndarray not as a pandas DataFrame\n",
    "    parser=\"auto\"\n",
    ")\n",
    "mnist.keys()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's import the dataset, inputs and labels:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X, y = mnist['data'], mnist['target'].astype(np.uint8)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Split the dataset in training and test set\n",
    "\n",
    "Well set aside 10,000 samples for testing purposes. The data set is already shuffled for us so we can just take the last 10,000 samples for our test set."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# use slicing to create training and test set\n",
    "X_train, X_test, y_train, y_test = X[:60000], X[60000:], y[:60000], y[60000:]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X_train.shape, X_test.shape, y_train.shape, y_test.shape"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Multiclass Classification"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When you need to distinguish more than two classes, you have a multiclass classification problem. ML learning algorthms, solve the multiclass problem mainly in three ways:\n",
    "* they support multi-class classification natively (e.g. SGD, Naive Bayes, Random Forests)\n",
    "* they are binary classifiers in one-vs-the-rest strategy (OvR) ($N$ classifiers are needed for $N$ classes)\n",
    "* they are binary classifiers in one-vs-one-strategy (OvO) ($N\\times N-1/2$ classifiers are needed for $N$ classes). Each classifier has to be trained only on the subset of the training set for the two classes that it must distinguish.\n",
    "\n",
    "Support Vector Machines are generally trained using the OvO approach though this is hidden from the user in scikit-learn. Let's see an example using a non-linear SVM:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from time import time\n",
    "from sklearn.metrics import accuracy_score\n",
    "from sklearn.svm import SVC\n",
    "\n",
    "np.random.seed(0)\n",
    "X_val = X_train[50_000:52_000]\n",
    "y_val = y_train[50_000:52_000]\n",
    "\n",
    "# Non-linear SVMs do not scale well with a large number of samples\n",
    "for n_samples in [1000, 2000, 4000, 8000]:\n",
    "    \n",
    "    svm_cl = SVC(\n",
    "        C=10,       # cost of misclassification (inversely proportional to the margin size)\n",
    "        gamma=0.001  # inversely proportional to the radius of the radial basis function\n",
    "    )\n",
    "    start = time()\n",
    "    svm_cl.fit(X_train[:n_samples], y_train[:n_samples])\n",
    "    print(f\"Trained on {n_samples} samples. Duration: {time() - start:.2f} s\")\n",
    "    y_val_pred = svm_cl.predict(X_val)\n",
    "    acc =  accuracy_score(y_val, y_val_pred)\n",
    "    print(f\"Val accuracy = {100*acc:.2f} %\")"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise:** The performance on a validation set does not look good at all. What could I do that could drastically improve it?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# write your solution here. Use as many cells as you see necessary.\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If rather than `.predict()` we use `.decision_function()` we get one score per class. The class with the highest score for a given sample is the one that is actually \"predicted\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scores = svm_cl.decision_function(X_train[:10])\n",
    "scores"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.argmax(scores, axis=1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "svm_cl.predict(X_train[:10])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "svm_cl.classes_"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise: (at home):** Train a linear SVM classifier (same `C` as the nonlinear one) on the entire MNIST dataset. How is it's performance in cross-validation when compared to the non-linear SVM?\n",
    "\n",
    "You could also create a pipeline with a [Nystroem kernel approximator](https://scikit-learn.org/stable/modules/generated/sklearn.kernel_approximation.Nystroem.html#sklearn.kernel_approximation.Nystroem) (`gamma` = 0.01) and the Linear SVC. This should approximate the behaviour of a nonliner SVM on the large dataset.\n",
    "\n",
    "See more on the user guide: https://scikit-learn.org/stable/modules/kernel_approximation.html#nystroem-kernel-approx"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Write your solution here"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### More classifiers\n",
    "\n",
    "Let's try another pair of classifiers. A Random Forest classifier and  SGD classifier, trained in 3-fold cross validation. Which one performs better?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.ensemble import RandomForestClassifier\n",
    "from sklearn.model_selection import cross_val_score\n",
    "\n",
    "forest_cl = RandomForestClassifier(n_estimators=100, random_state=77, n_jobs=-1)\n",
    "start = time()\n",
    "scores = cross_val_score(\n",
    "    forest_cl,\n",
    "    X_train,\n",
    "    y_train,\n",
    "    cv=3,\n",
    "    scoring='accuracy'\n",
    ")\n",
    "print(f\"Random Forest. Execution time {time() - start} s\")\n",
    "print(f\"CV accuracy: avg: {100*np.mean(scores):.2f}, std: {100*np.std(scores):.2f}\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.linear_model import SGDClassifier\n",
    "\n",
    "sgd_cl = SGDClassifier(\n",
    "    random_state=77,\n",
    "    tol=1e-3,\n",
    "    max_iter=2000,\n",
    "    n_jobs=-1\n",
    ")\n",
    "start = time()\n",
    "scores = cross_val_score(sgd_cl, X_train, y_train, cv=3, scoring='accuracy')\n",
    "print(f\"SGD Classifier. Execution time {time() - start} s\")\n",
    "print(f\"CV accuracy: avg: {100*np.mean(scores):.2f}, std: {100*np.std(scores):.2f}\")"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Our `sgd_cl` should be initialized with a higher max_iter (eg 2000), otherwise the algorithm might not converge. This is not done in the live demo because the execution time becomes considerable. \n",
    "\n",
    "Another useful approach (as seen with SVMs) is to scale the images:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.preprocessing import StandardScaler\n",
    "scaler = StandardScaler()\n",
    "X_train_scaled = scaler.fit_transform(X_train.astype(np.float64))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.model_selection import cross_val_predict\n",
    "\n",
    "sgd_cl = SGDClassifier(\n",
    "    random_state=77,\n",
    "    tol=1e-3,\n",
    "    max_iter=2000,\n",
    "    n_jobs=-1\n",
    ")\n",
    "start = time()\n",
    "y_train_pred_sgd = cross_val_predict(sgd_cl, X_train_scaled, y_train, cv=3)\n",
    "acc = accuracy_score(y_train, y_train_pred_sgd)\n",
    "print(f\"SGD Classifier. Execution time {time() - start} s\")\n",
    "print(f\"CV accuracy: avg: {100*np.mean(acc):.2f}\")"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Model Evaluation"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In a proper machine learning problem solving approach you would pre-process the input data (as shown in Class 2) and then try a few ML algorithms, also exploring the parameter space using `GridSearchCV` or `RandomizedSearchCV` (we will see this in one of the following classes).\n",
    "\n",
    "Hypothesising that you have done so, and that you have a good model, you will then want to evaluate its performance.\n",
    "Let's look a the confusion matrix, as a first step.\n",
    "\n",
    "** Exercise:** compute the confusion matrix in cross-validation `c_mat_cv`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# complete the solution here:\n",
    "\n",
    "\n",
    "c_mat_cv = ...\n",
    "print('Execution time {} s'.format(time() - start))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.matshow(c_mat_cv, cmap=\"jet\")\n",
    "ax = plt.gca()\n",
    "ax.set_title(f\"MNIST - SGD Classifier (acc={acc:.4f})\")\n",
    "ax.set_xlabel(\"Predicted label\")\n",
    "ax.set_ylabel(\"True label\")\n",
    "ax.set_xticks(np.arange(0, 10, 1))\n",
    "ax.set_yticks(np.arange(0, 10, 1))\n",
    "ax.set_xticklabels(np.arange(0, 10, 1))\n",
    "ax.set_yticklabels(np.arange(0, 10, 1))\n",
    "\n",
    "\n",
    "for i in range(0, 10):\n",
    "   for j in range(0, 10):\n",
    "      c = c_mat_cv[j, i]\n",
    "      ax.text(i, j, str(c), va='center', ha='center')\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can evaluate the performance of the predictor (precision, recall and F1 score) for each class, using the summary function `classification_report()`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.metrics import classification_report\n",
    "report = classification_report(y_train, y_train_pred_sgd)\n",
    "print(report)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let’s plot the errors. Firstly, we divide each value by the number of images in the corresponding class so that we compare error rates rather than absolute numbers of errors. Then we fill the values along the diagonal with zeros to keep only the classification errors."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "row_sums = c_mat_cv.sum(axis=1, keepdims=True)\n",
    "norm_c_mat = c_mat_cv / row_sums\n",
    "np.fill_diagonal(norm_c_mat, 0)\n",
    "\n",
    "plt.matshow(norm_c_mat, cmap=plt.cm.gray)\n",
    "ax = plt.gca()\n",
    "ax.set_title(\"MNIST - SGD Classifier - Normalized Confusion Errors\")\n",
    "ax.set_xlabel(\"Predicted label\")\n",
    "ax.set_ylabel(\"True label\")\n",
    "ax.set_xticks(np.arange(0, 10, 1))\n",
    "ax.set_yticks(np.arange(0, 10, 1))\n",
    "ax.set_xticklabels(np.arange(0, 10, 1))\n",
    "ax.set_yticklabels(np.arange(0, 10, 1))\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we can better see which type of errors our classifier makes. Which are these?"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise: (at home)** repeat the same operation of printing out the confusion matrix in 3-fold CV for the Linear SVM classifier with or without Nystroem approximation. Doe it make the same errors as the SGD? Why or why not?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# write your solution here\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Multilabel Classification\n",
    "\n",
    "\n",
    "Multilabel classification happens when you want to predict more than one label at the time. For instance we may create two labels `y_train_prime` and `y_train_odd` and train a Nearest Neighbour classifier that predicts both:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "y_train_prime = np.in1d(y_train, [2, 3, 5, 7])\n",
    "y_train_odd = y_train % 2 == 1\n",
    "y_multilabel = np.c_[y_train_prime, y_train_odd]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.neighbors import KNeighborsClassifier\n",
    "\n",
    "knn_clf = KNeighborsClassifier()\n",
    "knn_clf.fit(X_train, y_multilabel)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "knn_clf.predict(X_train[:10])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "y_train[:10], y_multilabel[:10, 0], y_multilabel[:10, 1]"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's evaluate the overall f1-score for both labels with \"macro\" averaging (unweighted average of F1 across all classes):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.metrics import f1_score\n",
    "\n",
    "\n",
    "y_train_knn_pred = cross_val_predict(knn_clf, X_train, y_multilabel, cv=3)\n",
    "f1 = f1_score(\n",
    "    y_multilabel,\n",
    "    y_train_knn_pred,\n",
    "    average=\"macro\"\n",
    ")\n",
    "\n",
    "print(f\"F-1 score = {f1:.4f}\")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
