## Import PyTorch and Lightning
import lightning.pytorch as pl
import torch
import torchmetrics
import torchvision
import torch.nn.functional as F

from torch import nn, optim, utils



class ConvMNISTClassifier(pl.LightningModule):
    def __init__(self):
        super().__init__()
        self.layer_1 = nn.Sequential(
            nn.Conv2d(in_channels=1, out_channels=32, kernel_size=3, padding=1),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2)
        )
        self.layer_2 = nn.Sequential(
            nn.Conv2d(in_channels=32, out_channels=64, kernel_size=3),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.MaxPool2d(2)
        )
        self.fc_1 = nn.Linear(in_features=64*6*6, out_features=600)
        self.fc_2 = nn.Linear(in_features=600, out_features=120)
        self.out = nn.Linear(in_features=120, out_features=10)
        self.dropout = nn.Dropout(p=0.25)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        """
        Forward pass for our Classifier
        """
        # convolutional layers with batch normalization and maxpooling
        x = self.layer_1(x)
        x = self.layer_2(x)
        # flatten tensor to pass it to linear layers
        x = torch.flatten(x, start_dim=1)
        # fully connected layers
        x = F.relu(self.fc_1(x))
        x = self.dropout(x)
        x = F.relu(self.fc_2(x))
        # NB: the softmax layer can be omitted as `cross_entropy()` will compute the softmax just the same
        x = self.out(x)
        return x


    def training_step(self, batch, batch_idx):
        x, y = batch
        logits = self.forward(x)
        loss = F.cross_entropy(logits, y)
        self.log("train_loss", loss, on_step=True, on_epoch=True)
        return loss

    def validation_step(self, val_batch, batch_idx):
        x, y = val_batch
        val_logits = self.forward(x)
        val_loss = F.cross_entropy(val_logits, y)
        self.log("val_loss", val_loss, on_epoch=True)
        val_accuracy = torchmetrics.functional.accuracy(
            val_logits,
            y,
            task="multiclass",
            num_classes=10,
        )
        self.log("val_accuracy", val_accuracy, on_epoch=True)

    def test_step(self, test_batch, batch_idx):
        x, y = test_batch
        test_logits = self.forward(x)
        test_loss = nn.functional.cross_entropy(test_logits, y)
        self.log("test_loss", test_loss)
        test_accuracy = torchmetrics.functional.accuracy(
            test_logits,
            y,
            task="multiclass",
            num_classes=10,
        )
        self.log("test_accuracy", test_accuracy)

    def prediction_step(self):
        pass

    def configure_optimizers(self):
        optimizer = optim.Adam(self.parameters(), lr=1e-03)
        return optimizer